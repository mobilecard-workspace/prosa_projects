package com.addcel.prosa.view;

import com.addcel.api.ui.Viewable;
import com.addcel.api.ui.uicomponents.CustomEditField;
import com.addcel.api.ui.uicomponents.ElementLabelField;
import com.addcel.api.ui.uicomponents.buttons.CustomSelectedButtonField;
import com.addcel.api.ui.uicomponents.buttons.CustomSelectedSizeButton;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.container.HorizontalFieldManager;


public class WModificarDatos extends Viewable implements FieldChangeListener{

	private ElementLabelField usuarioTxt;
	private ElementLabelField nombreTxt;
	private ElementLabelField apellido01Txt;
	private ElementLabelField apellido02Txt;
	private ElementLabelField celularTxt;
	private ElementLabelField emailTxt;

	private ElementLabelField tiendaTxt;
	private ElementLabelField rolTxt;
	
	private CustomEditField usuarioEdit;
	private CustomEditField nombreEdit;
	private CustomEditField apellido01Edit;
	private CustomEditField apellido02Edit;
	private CustomEditField celularEdit;
	private CustomEditField emailEdit;

	private ObjectChoiceField choiceFieldTienda;
	private ObjectChoiceField choiceFieldRol;

	CustomSelectedButtonField aceptar;
	CustomSelectedButtonField cancelar;
	
	
	public WModificarDatos(boolean isSetTitle, String title) {
		super(isSetTitle, title);

		
		CustomSelectedSizeButton activo = new CustomSelectedSizeButton("Activo", 3);
		CustomSelectedSizeButton bloqueado = new CustomSelectedSizeButton("Bloqueado", 3);
		CustomSelectedSizeButton borrado = new CustomSelectedSizeButton("Borrado", 3);
		
		HorizontalFieldManager manager = new HorizontalFieldManager();
		
		manager.add(activo);
		manager.add(bloqueado);
		manager.add(borrado);
		
		add(manager);
		
		add(new LabelField());
		
		usuarioTxt = new ElementLabelField("Usuario:");
		nombreTxt = new ElementLabelField("Nombre:");
		apellido01Txt = new ElementLabelField("Apellido paterno:");
		apellido02Txt = new ElementLabelField("Apellido materno:");
		celularTxt = new ElementLabelField("N�mero de celular:");
		emailTxt = new ElementLabelField("Correo electr�nico:");

		tiendaTxt = new ElementLabelField("Tienda:");
		rolTxt = new ElementLabelField("Rol:");

		usuarioEdit = new CustomEditField();
		nombreEdit = new CustomEditField();
		apellido01Edit = new CustomEditField();
		apellido02Edit = new CustomEditField();
		celularEdit = new CustomEditField();
		emailEdit = new CustomEditField();

		choiceFieldTienda = new ObjectChoiceField();
		choiceFieldRol = new ObjectChoiceField();
		
		cancelar = new CustomSelectedButtonField("Cancelar", ButtonField.CONSUME_CLICK);
		aceptar = new CustomSelectedButtonField("Actualizar", ButtonField.CONSUME_CLICK);
		
		HorizontalFieldManager fieldManager = new HorizontalFieldManager();
		fieldManager.add(cancelar);
		fieldManager.add(aceptar);
		
		add(usuarioTxt);
		add(usuarioEdit);
		add(nombreTxt);
		add(nombreEdit);
		add(apellido01Txt);
		add(apellido01Edit);
		add(apellido02Txt);
		add(apellido02Edit);
		add(celularTxt);
		add(celularEdit);
		add(emailTxt);
		add(emailEdit);
		
		add(tiendaTxt);
		add(choiceFieldTienda);
		add(rolTxt);
		add(choiceFieldRol);
		add(new LabelField());
		add(fieldManager);
	}
	
	
	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
		
	}

	protected void analyzeData(int request, Object object) {
		// TODO Auto-generated method stub
		
	}

}
