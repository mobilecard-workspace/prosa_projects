package com.addcel.api.services.login;

import com.addcel.api.dataaccess.DataAccessible;
import com.addcel.api.ui.Viewable;

public class DALoginService extends DataAccessible implements Runnable {

	
	private Viewable viewable;
	private String data;
	
	public DALoginService(Viewable viewable, String data) {
		
		super(viewable, data);

		this.viewable = viewable;
		this.data = data;
	}

	public void run() {

		execute(viewable, data);
	}

	protected void execute(Viewable viewable, String json) {
	}

}
