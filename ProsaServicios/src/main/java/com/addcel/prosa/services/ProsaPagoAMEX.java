package com.addcel.prosa.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.prosa.model.mapper.BitacorasMapper;
import com.addcel.prosa.model.mapper.ProsaMapper;
import com.addcel.prosa.model.vo.ProsaDetalleVO;
import com.addcel.prosa.model.vo.RespuestaAmexVO;
import com.addcel.prosa.model.vo.pagos.TBitacoraVO;
import com.addcel.prosa.model.vo.request.DatosPago;
import com.addcel.prosa.utils.AddCelGenericMail;
import com.addcel.prosa.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;

@Service
public class ProsaPagoAMEX {
	
	private static final Logger logger = LoggerFactory.getLogger(ProsaPagosService.class);
	private static final String urlStringAMEX = "http://localhost:8080/AmexWeb/AmexAuthorization";
//	private static final String urlStringAMEX = "http://50.57.192.210:8080/AmexWeb/AmexAuthorization";
	
	@Autowired
	private BitacorasMapper bm;
	@Autowired
	private UtilsService utilService;
	@Autowired
	private ProsaMapper mapperDF;
	
	private static final String patron = "ddhhmmss";
	private static final String patronCom = "yyyy-MM-dd hh:mm:ss";
	private static final SimpleDateFormat formatoCom = new SimpleDateFormat(patronCom);
	private static final SimpleDateFormat formato = new SimpleDateFormat(patron);
	
	public String procesaPago(String jsonEnc) {
		RespuestaAmexVO respuestaAmex = new RespuestaAmexVO();
		DatosPago datoAmex = null;
		String json = null;
		
		try{
			
			json=AddcelCrypto.decryptSensitive(jsonEnc);		
			logger.debug("jsondatospago: {}",json);
			datoAmex = (DatosPago) utilService.jsonToObject(json, DatosPago.class);
			datoAmex.setTipo("AMEX");
			
			if(datoAmex.getToken() == null){
				json = "{\"idError\":1,\"mensajeError\":\"El parametro TOKEN no puede ser NULL\"}";
				logger.error("El parametro TOKEN no puede ser NULL");
			}else{
				datoAmex.setToken(AddcelCrypto.decryptHard(datoAmex.getToken()));
				logger.info("token ==> {}",datoAmex.getToken());
				
				if((mapperDF.difFechaMin(datoAmex.getToken())) > 30){
//				if(false){
					json = "{\"idError\":2,\"mensajeError\":\"La Transacción ya no es válida\"}";
					logger.info("La Transacción ya no es válida");
				}else{
					logger.info("Inserta Bitacoras AMEX.");
					insertaBitacoras(datoAmex, datoAmex.getTotal(), 12.0);
					
					try {
						
						StringBuffer data = new StringBuffer() 
								.append( "tarjeta="    ).append( URLEncoder.encode(datoAmex.getTarjeta(), "UTF-8") )
								.append( "&vigencia="  ).append( URLEncoder.encode(datoAmex.getVigencia(), "UTF-8") )
								.append( "&monto="     ).append( URLEncoder.encode(String.valueOf(datoAmex.getTotal()), "UTF-8") )
								.append( "&cid="       ).append( URLEncoder.encode(datoAmex.getCvv2(), "UTF-8") )
								.append( "&direccion=" ).append( URLEncoder.encode(datoAmex.getDireccion(), "UTF-8") )
								.append( "&cp="        ).append( URLEncoder.encode(datoAmex.getCp(), "UTF-8") )
								.append( "&afiliacion=").append( URLEncoder.encode("00000000", "UTF-8"));
						
						logger.info("Env�o de datos AMEX: {}", data);
						
						URL url = new URL(urlStringAMEX);
						URLConnection urlConnection = url.openConnection();
						
						urlConnection.setDoOutput(true);
						
						OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
						wr.write(data.toString());
						wr.flush();
						logger.info("Datos enviados, esperando respuesta de AMEX");
						
						BufferedReader rd = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
						String line;
						StringBuilder sb = new StringBuilder();
						
						while ((line = rd.readLine()) != null) {
							sb.append(line);
						}
						
						wr.close();
						rd.close();
						
						respuestaAmex = (RespuestaAmexVO) utilService.jsonToObject(sb.toString(), RespuestaAmexVO.class);
						
						//*******************************************
						String patron = "000000";
				    	String patron2 = "000000000000";
				    	DecimalFormat formato = new DecimalFormat(patron);
				    	DecimalFormat formato2 = new DecimalFormat(patron2);
						Random  rnd = new Random();
				    	
				    	int aut = (int)(rnd.nextDouble() * 900000);
				    	long ref = (long)(rnd.nextDouble() * 900000000);
				    	
				    	//*******************************************
						
						logger.info("Respuesta: {}", sb.toString());
						logger.info("*****************CODIGO AMEX {}", respuestaAmex.getCode());
						if (respuestaAmex != null && respuestaAmex.getCode().equals("000")) { //Pago realizado con �xito
							logger.info("Pago realizado con �xito.");
							
							respuestaAmex.setTransaction(formato2.format(ref));
							
							datoAmex.setNoAutorizacion(respuestaAmex.getTransaction());
							datoAmex.setReferencia(String.valueOf(datoAmex.getIdBitacora()));
							datoAmex.setFecha(formatoCom.format(new Date()));
							datoAmex.setTransaccion(String.valueOf(datoAmex.getIdBitacora()));
							datoAmex.setStatus(1);
							datoAmex.setMsg("EXITO PAGO PROSA AMEX AUTORIZADA");
							updateBitacoras(datoAmex);
							
							json = "{\"idError\":0,\"mensajeError\":\"El pago fue exitoso.\",\"referencia\":" + datoAmex.getReferencia() + 
									",\"autorizacion\":" + datoAmex.getNoAutorizacion() +"}";
							
							AddCelGenericMail.sendMail(
									utilService.objectToJson(AddCelGenericMail.generatedMail(datoAmex)));
						} else {
							logger.info("El pago no se pudo realizar: {} ", respuestaAmex.getErrorDsc());
							datoAmex.setStatus(2);
							datoAmex.setMsg("ERROR EN EL PAGO PROSA AMEX");
							json = "{\"idError\":1,\"mensajeError\":\"Ocurrio un error: " + respuestaAmex.getErrorDsc() + ".\",\"referencia\":" + datoAmex.getNoAutorizacion() + "}";
							updateBitacoras(datoAmex);
						}
						
					} catch (Exception ex) {
						logger.error("Error al procesar pago AMEX: {}", ex);
						logger.info("El pago no se pudo realizar error {}", ex);
						datoAmex.setStatus(2);
						datoAmex.setMsg("ERROR EN EL PAGO PROSA AMEX");
						json = "{\"idError\":2,\"mensajeError\":\"Ocurrio un error durante el pago.\",\"referencia\":" + datoAmex.getNoAutorizacion() + "}";
						updateBitacoras(datoAmex);
					}
				}
			}
		}catch(Exception e){
			logger.error("Ocurrio un error:", e);
			datoAmex.setStatus(2);
			datoAmex.setMsg("ERROR EN EL PAGO PROSA AMEX");
			json = "{\"idError\":3,\"mensajeError\":\"Ocurrio un error.\",\"referencia\":" + datoAmex.getNoAutorizacion() + "}";
			updateBitacoras(datoAmex);
		}
		json = AddcelCrypto.encryptSensitive(formato.format(new Date()),json);
		return json;
	}
	
	private long insertaBitacoras(DatosPago dp, Double total, Double tipoCambio ){		
		TBitacoraVO tb = new TBitacoraVO(
				dp.getIdUser(),dp.getIdProveedor(), "PAGO PROSA AMEX", dp.getImei(), "PAGO PROSA AMEX",
				null,dp.getTipo(), dp.getSoftware(), dp.getModelo(),dp.getWkey());
		bm.addBitacora(tb);
		ProsaDetalleVO dfd = new ProsaDetalleVO(
				tb.getIdBitacora(), dp.getIdUser(),dp.getIdProveedor() ,dp.getDescProducto(),  
				total, dp.getMoneda(), tipoCambio, "AMEX", dp.getNombres(), dp.getEmail());
		bm.addProsaDetalle(dfd);
		dp.setIdBitacora(tb.getIdBitacora());
		
		return tb.getIdBitacora();
	}
	
	private void updateBitacoras(DatosPago dp){
		TBitacoraVO tb = new TBitacoraVO();
		
		tb.setIdBitacora(dp.getIdBitacora());
		tb.setBitStatus(dp.getStatus());
		tb.setBitConcepto(dp.getMsg());
		tb.setBitNoAutorizacion(dp.getNoAutorizacion());
		bm.updateBitacora(tb);
		// idStatus
		bm.updateProsaDetalle(dp.getIdBitacora(), dp.getStatus());
	}
}