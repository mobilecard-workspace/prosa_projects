package com.addcel.prosa.view;

import java.util.Vector;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BasicEditField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

import com.addcel.api.addcelexception.Error;
import com.addcel.api.dataaccess.DataAccessible;
import com.addcel.api.ui.Viewable;
import com.addcel.api.ui.uicomponents.CustomEditField;
import com.addcel.api.ui.uicomponents.ElementLabelField;
import com.addcel.api.ui.uicomponents.buttons.CustomSelectedButtonField;
import com.addcel.prosa.Start;
import com.addcel.prosa.dto.ContestacionAlta;
import com.addcel.prosa.dto.Rol;
import com.addcel.prosa.dto.Tienda;
import com.addcel.prosa.model.user.DAAltaUser;


public class WRegistro extends Viewable implements FieldChangeListener{


	private ElementLabelField usuarioTxt;
	private ElementLabelField nombreTxt;
	private ElementLabelField apellido01Txt;
	private ElementLabelField apellido02Txt;
	private ElementLabelField celularTxt;
	private ElementLabelField emailTxt;

	private ElementLabelField tiendaTxt;
	private ElementLabelField rolTxt;
	
	private CustomEditField usuarioEdit;
	private CustomEditField nombreEdit;
	private CustomEditField apellido01Edit;
	private CustomEditField apellido02Edit;
	private CustomEditField celularEdit;
	private CustomEditField emailEdit;

	private ObjectChoiceField choiceFieldTienda;
	private ObjectChoiceField choiceFieldRol;

	CustomSelectedButtonField aceptar;
	CustomSelectedButtonField cancelar;
	
	
	public WRegistro(boolean isSetTitle, String title) {
		super(isSetTitle, title);

		usuarioTxt = new ElementLabelField("Usuario:");
		nombreTxt = new ElementLabelField("Nombre:");
		apellido01Txt = new ElementLabelField("Apellido paterno:");
		apellido02Txt = new ElementLabelField("Apellido materno:");
		celularTxt = new ElementLabelField("N�mero de celular:");
		emailTxt = new ElementLabelField("Correo electr�nico:");

		tiendaTxt = new ElementLabelField("Tienda:");
		rolTxt = new ElementLabelField("Rol:");

		usuarioEdit = new CustomEditField();
		nombreEdit = new CustomEditField();
		apellido01Edit = new CustomEditField();
		apellido02Edit = new CustomEditField();
		celularEdit = new CustomEditField();
		emailEdit = new CustomEditField();

		choiceFieldTienda = new ObjectChoiceField();
		choiceFieldRol = new ObjectChoiceField();
		
		cancelar = new CustomSelectedButtonField("Cancelar", ButtonField.CONSUME_CLICK);
		aceptar = new CustomSelectedButtonField("Registrar", ButtonField.CONSUME_CLICK);
		
		cancelar.setChangeListener(this);
		aceptar.setChangeListener(this);
		
		HorizontalFieldManager fieldManager = new HorizontalFieldManager();
		fieldManager.add(cancelar);
		fieldManager.add(aceptar);
		
		add(usuarioTxt);
		add(usuarioEdit);
		add(nombreTxt);
		add(nombreEdit);
		add(apellido01Txt);
		add(apellido01Edit);
		add(apellido02Txt);
		add(apellido02Edit);
		add(celularTxt);
		add(celularEdit);
		add(emailTxt);
		add(emailEdit);
		
		add(tiendaTxt);
		add(choiceFieldTienda);
		add(rolTxt);
		add(choiceFieldRol);
		
		add(fieldManager);
	}

	
	private Object getObject(ObjectChoiceField objectChoiceField){
		
		int index = objectChoiceField.getSelectedIndex();
		Object object = objectChoiceField.getChoice(index);
		return object; 
	}
	
	
	public void fieldChanged(Field field, int context) {

		if (field == cancelar){
			
			UiApplication.getUiApplication().popScreen(this);
		} else if (field == aceptar) {
			
		
			JSONObject jsonObject = new JSONObject();

			try {
				jsonObject.put("nombres", nombreEdit.getText());
				jsonObject.put("apellidoP", apellido01Edit.getText());
				jsonObject.put("apellidoM", apellido02Edit.getText());
				jsonObject.put("email", emailEdit.getText());
				jsonObject.put("telefono", celularEdit.getText());
				jsonObject.put("idProveedor", "20");
				jsonObject.put("idSupervisor", String.valueOf(Start.respuestaLogin.getIdUser()));
				jsonObject.put("login", usuarioEdit.getText());
				
				Rol rol = (Rol)getObject(choiceFieldRol);
				
				jsonObject.put("idRole", String.valueOf(rol.getIdRol()));
				
				Tienda tienda = (Tienda)getObject(choiceFieldTienda);
				jsonObject.put("idTienda", String.valueOf(tienda.getIdTienda()));
				
				Thread thread = new Thread(new DAAltaUser(this, jsonObject.toString()));
				thread.start();
			} catch (JSONException e) {
				e.printStackTrace();
				Dialog.alert(Error.JSON_EXCEPTION);
			}
		}
	}

	protected void analyzeData(int request, Object object) {

		if (request == DataAccessible.ERROR){
			
			String message = cleanMessageError(object);
			Dialog.alert(message);
		} else if (request == DataAccessible.DATA){
			
			if (object instanceof Vector){
				
				Vector vector = (Vector)object;
				
				int size = vector.size();
				
				if (size >= 1){
					
					Object object2 = vector.elementAt(0);
					
					if (object2 instanceof Rol){
						
						Rol roles[] = new Rol[size];
						
						vector.copyInto(roles);
						
						choiceFieldRol.setChoices(roles);
						
					} else if (object2 instanceof Tienda){
						
						Tienda tiendas[] = new Tienda[size];
						
						vector.copyInto(tiendas);
						
						choiceFieldTienda.setChoices(tiendas);
					}
				}
				
			} else if(object instanceof ContestacionAlta){
				
				ContestacionAlta alta = (ContestacionAlta)object;
				Dialog.alert(alta.getMensajeError());
			}
		}
	}

	
	
	
	private boolean checarCampos(){
		
		boolean check = true;
		
		if (invalidText(usuarioEdit)){
			check = false;
		} else if (invalidText(nombreEdit)){
			check = false;
		} else if (invalidText(apellido01Edit)){
			check = false;
		} else if (invalidText(apellido02Edit)){
			check = false;
		} else if (invalidText(celularEdit)){
			check = false;
		} else if (invalidText(emailEdit)){
			check = false;
		}
		
		return check;
	}
	

	private boolean invalidText(BasicEditField editField){
		
		String temp = editField.getText();
		
		if ((temp != null)&&(temp.length()>0)){
			return false;
		} else {
			return true;
		}
	}
	
}
