package com.addcel.api.dataaccess.components.connection.http;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;

import com.addcel.api.addcelexception.Error;
import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.components.connection.Connectable;
import com.addcel.prosa.Start;

/*
	Some other notes on GET requests:
	
	GET requests can be cached
	GET requests remain in the browser history
	GET requests can be bookmarked
	GET requests should never be used when dealing with sensitive data
	GET requests have length restrictions
	GET requests should be used only to retrieve data
*/

public class MethodGET implements Connectable {

	private HttpConnection connection;
	private InputStream inputStream;
	private String url = null;
	private String data = null;
	//private Viewable viewable = null;
	
	StringBuffer stringBuffer = null;
	
	public MethodGET(String url){
		this.url = url;
		//this.viewable = viewable;
	}
	
	public String createURL() {
		return url + "?json=" + data + ";" + Start.IDEAL_CONNECTION + ";" + "ConnectionTimeout=30000";
	}
	
	public void execute(String json) throws OwnException {

		this.data = json;
		byte datas[] = null;
		ByteArrayOutputStream responseBytes = null;
		DataInputStream in = null;
		try {

			String id = "0";
			stringBuffer = new StringBuffer();
			
			connection = (HttpConnection) Connector.open(createURL(),
					Connector.READ_WRITE, true);

			connection.setRequestMethod(HttpConnection.GET);
			connection.setRequestProperty("User-Agent",
					"Profile/MIDP-1.0 Confirguration/CLDC-1.0");
			connection.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");


			in = connection.openDataInputStream();

			int length = (int) connection.getLength();

			if (length != -1) {
				datas = new byte[length];
				in.readFully(datas);
			} else {// Length not available...
				responseBytes = new ByteArrayOutputStream();
				int ch;
				while ((ch = in.read()) != -1) {
					responseBytes.write(ch);
				}
				datas = responseBytes.toByteArray();
			}
			
			this.data = new String(datas);
			
			System.out.println(this.data);
			
		} catch (IOException e) {
			throw new OwnException(Error.HTTP_SEND_DATA);
		} finally {

			try {
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}


	public Object getData() {
		return data;
	}
}

