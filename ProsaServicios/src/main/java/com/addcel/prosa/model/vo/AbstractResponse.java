package com.addcel.prosa.model.vo;

public class AbstractResponse {	
	private int idError;
	private String mensajeError;
	
	public AbstractResponse() {}
	
	public AbstractResponse(int idError, String mensajeError) {	
		this.idError = idError;
		this.mensajeError = mensajeError;
	}
	public int getIdError() {
		return idError;
	}
	public void setIdError(int idError) {
		this.idError = idError;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}	
}
