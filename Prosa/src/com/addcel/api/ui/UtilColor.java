package com.addcel.api.ui;


public class UtilColor {
	
	private final static int GREY_02 = 0xd1d7d2;
	private final static int GREY_04 = 0x939598;
	private final static int GREY_05 = 0x231f20;

	private final static int blanco = 0xFFFFFF;
	private final static int gris_claro = 0xE5E5E5;
	private final static int gris_oscuro = 0x333333;
	private final static int negro = 0x000000;
	private final static int rojo = 0xCC0000;

	public final static int MAIN_BACKGROUND = gris_claro;

	public final static int TITLE_STRING = blanco;
	public final static int TITLE_BACKGROUND = rojo;

	public final static int BUTTON_FOCUS = rojo;
	public final static int BUTTON_SELECTED = rojo;
	public final static int BUTTON_UNSELECTED = GREY_04;

	public final static int BUTTON_STRING_FOCUS = gris_claro;
	public final static int BUTTON_STRING_SELECTED = blanco;
	public final static int BUTTON_STRING_UNSELECTED = blanco;

	public final static int SUBTITLE_STRING = gris_claro;
	public final static int SUBTITLE_BACKGROUND = rojo;

	public final static int ELEMENT_STRING = gris_oscuro;
	public final static int ELEMENT_BACKGROUND = gris_claro;

	public final static int EDIT_TEXT_DATA_FOCUS = GREY_05;
	public final static int EDIT_TEXT_DATA_UNFOCUS = GREY_04;
	
	public final static int EDIT_TEXT_BACKGROUND_FOCUS = blanco;
	public final static int EDIT_TEXT_BACKGROUND_UNFOCUS = GREY_02;

	public final static int LIST_DESCRIPTION_TITLE = gris_oscuro;
	public final static int LIST_DESCRIPTION_DATA = GREY_05;

	public final static int LIST_BACKGROUND_SELECTED = gris_oscuro;
	public final static int LIST_BACKGROUND_UNSELECTED = gris_claro;


	public static int toRGB(int r, int g, int b){
		return (r<<16)+(g<<8)+b;		 
	}
}
