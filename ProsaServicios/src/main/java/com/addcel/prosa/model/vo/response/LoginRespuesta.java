package com.addcel.prosa.model.vo.response;

import java.util.Date;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.addcel.prosa.model.vo.AbstractResponse;
import com.addcel.prosa.model.vo.ComisionVO;
import com.addcel.prosa.model.vo.ModuleVO;
import com.addcel.prosa.model.vo.TiendaVO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginRespuesta extends AbstractResponse{	
	
	private int idUser;
	private String nombres;
	private String apellidoP;
	private String apellidoM;
	private String email;
	private Date lastLogin;
	private int loginCount;
	private int idStatus;
	private int idRol;
	private String login;
	private int idSupervisor;
	private int idProveedor;
	
	private TiendaVO tienda;
	private ComisionVO comisiones;
	private List<ModuleVO> modulos;
	private Double tipoCambio;
	
	public int getIdRol() {
		return idRol;
	}
	public void setIdRol(int idRol) {
		this.idRol = idRol;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public Date getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}
	public int getLoginCount() {
		return loginCount;
	}
	public void setLoginCount(int loginCount) {
		this.loginCount = loginCount;
	}
	public int getIdStatus() {
		return idStatus;
	}
	public void setIdStatus(int idStatus) {
		this.idStatus = idStatus;
	}
	public TiendaVO getTienda() {
		return tienda;
	}
	public void setTienda(TiendaVO tienda) {
		this.tienda = tienda;
	}	
	public ComisionVO getComisiones() {
		return comisiones;
	}
	public void setComisiones(ComisionVO comisiones) {
		this.comisiones = comisiones;
	}
	public List<ModuleVO> getModulos() {
		return modulos;
	}
	public void setModulos(List<ModuleVO> modulos) {
		this.modulos = modulos;
	}	
	public Double getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(Double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidoP() {
		return apellidoP;
	}
	public void setApellidoP(String apellidoP) {
		this.apellidoP = apellidoP;
	}
	public String getApellidoM() {
		return apellidoM;
	}
	public void setApellidoM(String apellidoM) {
		this.apellidoM = apellidoM;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public int getIdSupervisor() {
		return idSupervisor;
	}
	public void setIdSupervisor(int idSupervisor) {
		this.idSupervisor = idSupervisor;
	}
	public int getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}
}
