package com.addcel.prosa.model.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.dao.DuplicateKeyException;

import com.addcel.prosa.model.vo.ComisionVO;
import com.addcel.prosa.model.vo.LoginVO;
import com.addcel.prosa.model.vo.RoleVO;
import com.addcel.prosa.model.vo.TiendaVO;
import com.addcel.prosa.model.vo.UserVO;
import com.addcel.prosa.model.vo.response.DetalleVO;
import com.addcel.prosa.model.vo.response.LoginRespuesta;

public interface ProsaMapper {
	int addUser(UserVO usuario);

	LoginRespuesta loginUsuario(UserVO user);

	List<TiendaVO> listaTiendas(@Param(value = "proveedor") String proveedor);

	int insertaModulosUsuario(@Param(value = "idUser") int idUser,
			@Param(value = "idRol") int idRol,
			@Param(value = "idProveedor") int idProveedor
			) throws DuplicateKeyException;

	int updateUser(UserVO usuario);

	List<UserVO> findUser(@Param(value = "nombres") String nombres,
			@Param(value = "paterno") String paterno,
			@Param(value = "materno") String materno,
			@Param(value = "login") String login,
			@Param(value = "idSupervisor") String idSupervisor,
			@Param(value = "idProveedor") String idProveedor,
			@Param(value = "idTienda") String idTienda
			);
	
	List<UserVO> getVendor(@Param(value = "nombres") String nombres,
			@Param(value = "paterno") String paterno,
			@Param(value = "materno") String materno,
			@Param(value = "login") String login);

	List<RoleVO> getCatRoles(@Param(value = "aplicacion") String aplicacion,
			@Param(value = "proveedor") String proveedor);

	String getFechaActual();

	int difFechaMin(String fechaToken);

	int getIdUser(int idUser);

	List<DetalleVO> getDetalle(@Param(value = "idSupervisor") String idSupervisor,
			@Param(value = "idUser") String idUser,
			@Param(value = "idProveedor") String idProveedor,
			@Param(value = "idTienda") String idTienda,
			@Param(value = "fi") String fechaInicial,
			@Param(value = "ff") String fechaFinal,
			@Param(value = "idRol") String idRol,
			@Param(value = "idBitacora") String idBitacora);

	UserVO userIntentosLogin(LoginVO loginVO);

	int updateIntentosStatus(@Param(value = "idUser") int idUser,
			@Param(value = "idProveedor") int idProveedor,
			@Param(value = "valor") int valor,
			@Param(value = "update") String update);
	
	ComisionVO getComisiones(@Param(value = "idProveedor")int idProveedor);
}
