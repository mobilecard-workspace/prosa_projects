package com.addcel.prosa.model.vo.request;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class RequestVendedor implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String idSupervisor;
	private String idUser;
	private String idTienda;
	private String idProveedor;
	public String getIdSupervisor() {
		return idSupervisor;
	}
	public void setIdSupervisor(String idSupervisor) {
		this.idSupervisor = idSupervisor;
	}
	public String getIdUser() {
		return idUser;
	}
	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}
	public String getIdTienda() {
		return idTienda;
	}
	public void setIdTienda(String idTienda) {
		this.idTienda = idTienda;
	}
	public String getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(String idProveedor) {
		this.idProveedor = idProveedor;
	}

}