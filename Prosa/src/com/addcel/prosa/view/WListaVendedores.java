package com.addcel.prosa.view;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;

import com.addcel.api.ui.Viewable;
import com.addcel.prosa.dto.Vendedor;
import com.addcel.prosa.view.base.uicomponents.vendedores.CustomGridFieldVendedores;

public class WListaVendedores extends Viewable implements FieldChangeListener{

	public WListaVendedores(boolean isSetTitle, String title) {
		
		super(isSetTitle, title);
		
		CustomGridFieldVendedores option01 = new CustomGridFieldVendedores(new Vendedor());
		CustomGridFieldVendedores option02 = new CustomGridFieldVendedores(new Vendedor());
		CustomGridFieldVendedores option03 = new CustomGridFieldVendedores(new Vendedor());
		
		add(option01);
		add(option02);
		add(option03);
	}

	public void fieldChanged(Field field, int context) {

		
	}

	protected void analyzeData(int request, Object object) {

		
	}

}
