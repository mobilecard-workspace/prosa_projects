package com.addcel.api.ui.uicomponents.splash;

import java.util.TimerTask;

import com.addcel.prosa.view.WLogin;

import net.rim.device.api.ui.UiApplication;

public class Wait extends TimerTask {

	private Admin viewSplash;

	public Wait(Admin viewSplash) {

		this.viewSplash = viewSplash;
	}

	public void run() {

		UiApplication.getUiApplication().invokeAndWait(new Runnable() {
			public void run() {
				UiApplication.getUiApplication().popScreen(viewSplash);
				UiApplication.getUiApplication().pushScreen(new WLogin(true, "Autentificación"));
			}
		});
	}
}