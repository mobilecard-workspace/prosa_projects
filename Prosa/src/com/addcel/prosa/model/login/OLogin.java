package com.addcel.prosa.model.login;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.addcelexception.Error;
import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.components.toObject.Objectable;
import com.addcel.prosa.dto.Comision;
import com.addcel.prosa.dto.Modulo;
import com.addcel.prosa.dto.Moneda;
import com.addcel.prosa.dto.RespuestaLogin;
import com.addcel.prosa.dto.Tienda;

public class OLogin implements Objectable{

	private JSONObject jsonObject = null;
	RespuestaLogin respuestaLogin = null;
	
	public Object execute(String data) throws OwnException {

		
		try {
			
			jsonObject = new JSONObject(data);
			
			respuestaLogin = new RespuestaLogin();
			
			respuestaLogin.setIdError(jsonObject.optInt("idError", 1));
			
			if ((respuestaLogin.getIdError() == 0)||(respuestaLogin.getIdError() == 2)){
				
				respuestaLogin.setTipoCambio(jsonObject.optInt("tipoCambio", 0));
				respuestaLogin.setIdUser(jsonObject.optInt("idUser", 0));
				respuestaLogin.setLoginCount(jsonObject.optInt("loginCount", 0));
				respuestaLogin.setIdStatus(jsonObject.optInt("idStatus", 0));
				respuestaLogin.setMensajeError(jsonObject.optString("mensajeError", null));
				respuestaLogin.setLastLogin(jsonObject.optString("lastLogin", null));
				respuestaLogin.setIdRol(jsonObject.optInt("idRol", 0));
				
				
				
				JSONObject tienda = jsonObject.optJSONObject("tienda");
				
				if (tienda != null){

					Tienda tienda2 = new Tienda();
					
					tienda2.setDescripcion(tienda.optString("descripcion", null));
					tienda2.setIdProveedor(tienda.optInt("idProveedor", 0));
					tienda2.setIdTienda(tienda.optInt("idTienda", 0));
					
					respuestaLogin.setTienda(tienda2);
				}
				
				
				
				JSONObject comisiones = jsonObject.optJSONObject("comisiones");
				
				if (comisiones != null){

					Comision comision = new Comision();

					comision.setComision(comisiones.optDouble("comision", 0));
					comision.setComisionPorcentaje(comisiones.optDouble("comisionPorcentaje", 0));
					comision.setMinCom(comisiones.optDouble("minCom", 0));
					comision.setMinComPorcentaje(comisiones.optDouble("minComPorcentaje", 0));
					
					respuestaLogin.setComisiones(comision);
				}
				
				
				
				JSONArray modulos = jsonObject.optJSONArray("modulos");
				
				if (modulos != null){

					int length = modulos.length();
					
					Vector vector = new Vector();
					
					for(int index = 0; index < length; index++){
						
						JSONObject jsonObject = (JSONObject)modulos.get(index);

						Modulo modulo = new Modulo();
						
						modulo.setIdModule(jsonObject.optInt("idModule", 0));
						modulo.setDescripcion(jsonObject.optString("descripcion", null));
						
						vector.addElement(modulo);
					}

					if (vector.size() > 0){
						respuestaLogin.setModulos(vector);
					}
				}


				JSONArray monedas = jsonObject.optJSONArray("moneda");
				
				if (monedas != null){
					
					int length = monedas.length();
					Vector vector = new Vector();
					
					for(int index = 0; index < length; index++){
						
						JSONObject jsonObject = (JSONObject)monedas.get(index);

						Moneda moneda = new Moneda();

						moneda.setIdMoneda(jsonObject.optInt("idMoneda", 0));
						moneda.setDescripcion(jsonObject.optString("descripcion", null));
						
						vector.addElement(moneda);
					}
					
					
					if (vector.size() > 0){
						respuestaLogin.setMonedas(vector);
					}
				}

			} else {
				respuestaLogin.setMensajeError(jsonObject.optString("mensajeError", null));
			}
			
			return respuestaLogin;
			
		} catch (JSONException e) {
			e.printStackTrace();
			throw new OwnException(Error.JSON_EXCEPTION);
		}
	}
}
