package com.addcel.prosa.model.vo;

public class RoleVO {
	private int idRol;
	private String Descripcion;
	private String maxLogin;
	private int idAplicacion;
	public int getIdRol() {
		return idRol;
	}
	public void setIdRol(int idRol) {
		this.idRol = idRol;
	}
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	public String getMaxLogin() {
		return maxLogin;
	}
	public void setMaxLogin(String maxLogin) {
		this.maxLogin = maxLogin;
	}
	public int getIdAplicacion() {
		return idAplicacion;
	}
	public void setIdAplicacion(int idAplicacion) {
		this.idAplicacion = idAplicacion;
	}
}
