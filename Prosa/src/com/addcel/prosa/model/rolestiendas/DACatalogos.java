package com.addcel.prosa.model.rolestiendas;

import java.util.Vector;

import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.DataAccessible;
import com.addcel.api.dataaccess.components.connection.http.MethodPOST;
import com.addcel.api.ui.Viewable;
import com.addcel.prosa.model.URL;

public class DACatalogos extends DataAccessible implements Runnable{
	
	public DACatalogos(Viewable viewable, String data) {
		super(viewable, data);
	}

	protected void execute(Viewable v, String json) {

		try {
			
			String encrypt = this.encrypt(CRYPTO_HARD, "{\"id_aplicacion\":\"1\",\"id_proveedor\":\"20\"}");
			
			connectable = new MethodPOST(URL.URL_GET_CATALOGO_ROLES);
			connectable.execute(encrypt);
			
			String httpAnswer = (String)connectable.getData();
			
			String decrypt = this.decrypt(CRYPTO_HARD, httpAnswer);
			
			ORoles oRoles = new ORoles();
			Vector roles = (Vector)oRoles.execute(decrypt);
			
			viewable.setData(DataAccessible.DATA, roles);
			
			encrypt = this.encrypt(CRYPTO_HARD, "{\"proveedor\":\"20\"}");
			
			connectable = new MethodPOST(URL.URL_GET_CATALOGO_TIENDAS);
			connectable.execute(encrypt);
			
			httpAnswer = (String)connectable.getData();
			
			decrypt = this.decrypt(CRYPTO_HARD, httpAnswer);
			
			
			OTiendas oTiendas = new OTiendas();
			Vector tiendas = (Vector)oTiendas.execute(decrypt);
			
			viewable.setData(DataAccessible.DATA, tiendas);
			
		} catch (OwnException e) {
			viewable.setData(DataAccessible.ERROR, e.toString());
		}
	}

	public void run() {
		
		execute(viewable, data);
	}
}
