package com.addcel.prosa.model.login;

import com.addcel.api.dataaccess.components.encryption.Encryptionable;
import com.addcel.api.util.security.AddcelCrypto;


public class EncryptLogin implements Encryptionable {

	String data = null;
	
	public Object execute(String data) {

		return AddcelCrypto.encryptHard(data);
	}
}
