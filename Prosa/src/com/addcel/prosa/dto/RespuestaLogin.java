package com.addcel.prosa.dto;

import java.util.Vector;

public class RespuestaLogin {

	/*
	vendedor 2
	administrador 1
	*/
	
	public static final int USER_ADMINISTRADOR = 1;
	public static final int USER_VENDEDOR = 2;
	
	private String user;
	private String password;
	
	private int idError;
	private int tipoCambio;
	private int idUser;
	private int loginCount;
	private int idStatus;
	private int idRol; 

	private String mensajeError;
	private String lastLogin;
	
	private Vector monedas;
	private Vector modulos;
	private Comision comisiones;
	private Tienda tienda;

	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Vector getMonedas() {
		return monedas;
	}
	public void setMonedas(Vector monedas) {
		this.monedas = monedas;
	}
	public Vector getModulos() {
		return modulos;
	}
	public void setModulos(Vector modulos) {
		this.modulos = modulos;
	}
	public Comision getComisiones() {
		return comisiones;
	}
	public void setComisiones(Comision comisiones) {
		this.comisiones = comisiones;
	}
	public Tienda getTienda() {
		return tienda;
	}
	public void setTienda(Tienda tienda) {
		this.tienda = tienda;
	}
	
	public int getIdError() {
		return idError;
	}
	public void setIdError(int idError) {
		this.idError = idError;
	}
	public int getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(int tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public int getLoginCount() {
		return loginCount;
	}
	public void setLoginCount(int loginCount) {
		this.loginCount = loginCount;
	}
	public int getIdStatus() {
		return idStatus;
	}
	public void setIdStatus(int idStatus) {
		this.idStatus = idStatus;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	public String getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(String lastLogin) {
		this.lastLogin = lastLogin;
	}
	public int getIdRol() {
		return idRol;
	}
	public void setIdRol(int idRol) {
		this.idRol = idRol;
	}
}
