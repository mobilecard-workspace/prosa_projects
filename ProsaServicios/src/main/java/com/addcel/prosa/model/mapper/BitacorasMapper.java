package com.addcel.prosa.model.mapper;

import org.apache.ibatis.annotations.Param;

import com.addcel.prosa.model.vo.ProsaDetalleVO;
import com.addcel.prosa.model.vo.pagos.TBitacoraProsaVO;
import com.addcel.prosa.model.vo.pagos.TBitacoraVO;

public interface BitacorasMapper {
	int addBitacoraProsa(TBitacoraProsaVO b);
	int addBitacora(TBitacoraVO b);
	int updateBitacoraProsa(TBitacoraProsaVO b);
	int updateBitacora(TBitacoraVO b);
	int addProsaDetalle(ProsaDetalleVO dfd);
	int updateProsaDetalle(@Param(value="idBitacora")long idBitacora,@Param(value="status")int status);
}
