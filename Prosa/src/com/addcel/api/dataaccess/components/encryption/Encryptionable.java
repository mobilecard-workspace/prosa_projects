package com.addcel.api.dataaccess.components.encryption;

public interface Encryptionable {

	public Object execute(String data);
}
