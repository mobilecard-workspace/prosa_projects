package com.addcel.prosa.view.base.uicomponents.menu;


import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.VerticalFieldManager;

import com.addcel.api.ui.UtilColor;
import com.addcel.prosa.model.rolestiendas.DACatalogos;
import com.addcel.prosa.view.WBusqueda;
import com.addcel.prosa.view.WMenuAdministrador;
import com.addcel.prosa.view.WModificarContrasenia;
import com.addcel.prosa.view.WModificarNuevaContrasenia;
import com.addcel.prosa.view.WPagoAmex;
import com.addcel.prosa.view.WPagoVISA;
import com.addcel.prosa.view.WRegistro;
import com.addcel.prosa.view.WVentasVendedor;

public class CustomGridFieldMenu extends VerticalFieldManager {

	public final static int REGISTRO = 1;
	public final static int BUSQUEDA = 2;
	public final static int CONSULTAS = 3;
	public final static int CAMBIO_PASSWORD = 4;
	public final static int PAGO_VISA_MASTER = 5;
	public final static int PAGO_AMEX = 6;
	public final static int CONSULTA_VENTAS = 7;
	
	public static String S_REGISTRO = "Registro";
	public static String S_BUSQUEDA = "Busqueda";
	public static String S_CONSULTAS = "Consultas";
	public static String S_CAMBIO_PASSWORD = "Cambio de password";
	public static String S_PAGO_VISA_MASTER = "Pago VISA/Mastercard";
	public static String S_PAGO_AMEX = "Pago Amex";
	public static String S_CONSULTA_VENTAS = "Consulta ventas";
	
	private int option;
	
	
	public CustomGridFieldMenu(String name, int option) {

		super();

		this.option = option;
		
		OptionRichTextField option1 = new OptionRichTextField(name);

		add(option1);
		add(new NullField());
	}

	protected void paint(Graphics graphics) {

		graphics.clear();

		if (isFocus()) {
			graphics.setColor(UtilColor.LIST_BACKGROUND_SELECTED);
			graphics.setGlobalAlpha(70);
			graphics.fillRoundRect(0, 0, getPreferredWidth(),
					getPreferredHeight(), 1, 1);
			graphics.setGlobalAlpha(255);
		} else {
			graphics.setColor(UtilColor.LIST_BACKGROUND_UNSELECTED);
			graphics.fillRoundRect(0, 0, getPreferredWidth(),
					getPreferredHeight(), 1, 1);
		}

		super.paint(graphics);
	}

	public boolean isFocusable() {
		return true;
	}

	public int getPreferredWidth() {
		return Display.getWidth();
	}

	protected boolean navigationClick(int status, int time) {
		fieldChangeNotify(0);
		showView();
		return super.navigationClick(status, time);
	}

	protected boolean keyChar(char character, int status, int time) {
		if (character == Keypad.KEY_ENTER) {
			fieldChangeNotify(0);
			showView();
			return true;
		}

		/*
		 * else if (character == Keypad.KEY_ESCAPE){ Dialog.alert("Salir"); }
		 */
		return super.keyChar(character, status, time);
	}

	protected boolean touchEvent(TouchEvent message) {

		if (message.getEvent() == TouchEvent.CLICK) {
			fieldChangeNotify(0);
			showView();
			return true;
		}

		return super.touchEvent(message);
	}

	protected void onFocus(int direction) {
		super.onFocus(direction);
		invalidate();
	}

	protected void onUnfocus() {
		super.onUnfocus();
		invalidate();
	}
	
	private void showView(){

		switch (option) {
		
		case REGISTRO:
			
			WRegistro wRegistro = new WRegistro(true, "Opciones");
			UiApplication.getUiApplication().pushScreen(wRegistro);
			Thread thread = new Thread(new DACatalogos(wRegistro, null));
			thread.start();
			break;
		case BUSQUEDA:
			UiApplication.getUiApplication().pushScreen(new WBusqueda(true, "Busqueda"));
			break;
		case CONSULTAS:
			
			break;
		case CAMBIO_PASSWORD:
			UiApplication.getUiApplication().pushScreen(new WModificarContrasenia(true, "Modificar contraseņa"));
			break;
		case PAGO_VISA_MASTER:
			UiApplication.getUiApplication().pushScreen(new WPagoVISA(true, "Pago VISA"));
			break;
		case PAGO_AMEX:
			UiApplication.getUiApplication().pushScreen(new WPagoAmex(true, "Pago AMEX"));
			break;
		case CONSULTA_VENTAS:
			UiApplication.getUiApplication().pushScreen(new WVentasVendedor(true, "Ventas"));
			break;
		default:
			break;
		}
		//UiApplication.getUiApplication().pushScreen(new ViewPassengers(isDepartFlight, isBasicFlight));
	}
}
