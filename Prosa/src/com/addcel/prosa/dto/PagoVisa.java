package com.addcel.prosa.dto;

public class PagoVisa {

	//{"idError":0,"mensajeError":"�xito en el pago." ,"referencia":"125","autorizacion":"16109"}
	
	private int idError;
	private String mensajeError;
	private String referencia;
	private String autorizacion;
	
	
	public int getIdError() {
		return idError;
	}
	public void setIdError(int idError) {
		this.idError = idError;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getAutorizacion() {
		return autorizacion;
	}
	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}
}
