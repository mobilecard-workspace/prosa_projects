package com.addcel.api.ui.uicomponents;

import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.RadioButtonField;
import net.rim.device.api.ui.component.RadioButtonGroup;

import com.addcel.api.ui.UtilColor;

public class CustomRadioButtonField extends RadioButtonField{

	
	public CustomRadioButtonField(String label, RadioButtonGroup group, boolean selected) {
		
		super(label, group, selected);
	}
	
	protected void paint(Graphics graphics){
		
		graphics.setColor(UtilColor.ELEMENT_STRING);
		super.paint(graphics);
	}
	
}
