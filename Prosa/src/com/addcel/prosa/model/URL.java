package com.addcel.prosa.model;

public class URL {

	public static String URL_GET_LOGIN = "http://50.57.192.213:8080/ProsaServicios/login";
	public static String URL_GET_CATALOGO_TIENDAS = "http://50.57.192.213:8080/ProsaServicios/getTiendas";
	public static String URL_GET_CATALOGO_ROLES = "http://50.57.192.213:8080/ProsaServicios/getRoles";
	
	public static String URL_AGREGAR_USUARIOS = "http://50.57.192.213:8080/ProsaServicios/addUser";
	
	public static String URL_CAMBIAR_STATUS_USUARIOS = "http://50.57.192.213:8080/ProsaServicios/updatePassStatus";
	
	public static String URL_CAMBIAR_CONTRASENIA = "http://50.57.192.213:8080/ProsaServicios/updatePass";
	
	
	
	public static String URL_GET_TOKEN = "http://50.57.192.213:8080/ProsaServicios/getToken";
	
	public static String URL_PAGO_VISA = "http://50.57.192.213:8080/ProsaServicios/pago-visa";
	
	public static String URL_PAGO_AMEX = "http://50.57.192.213:8080/ProsaServicios/pago-amex";
}
