package com.addcel.prosa.model.login;

import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.DataAccessible;
import com.addcel.api.dataaccess.components.connection.http.MethodPOST;
import com.addcel.api.ui.Viewable;
import com.addcel.prosa.dto.RespuestaLogin;
import com.addcel.prosa.model.URL;

public class DALogin extends DataAccessible implements Runnable {

	public DALogin(Viewable viewable, String data) {
		super(viewable, data);
	}

	public void run() {
		execute(viewable, data);
	}

	public void execute(Viewable viewable, String json) {

		try {

			String encrypt = this.encrypt(CRYPTO_HARD, json);

			connectable = new MethodPOST(URL.URL_GET_LOGIN);
			connectable.execute(encrypt);
			String descrypt = (String)connectable.getData();
			
			descrypt = decrypt(CRYPTO_HARD, descrypt);
			
			OLogin login = new OLogin();
			RespuestaLogin respuestaLogin = (RespuestaLogin)login.execute(descrypt);
			
			viewable.setData(DataAccessible.DATA, respuestaLogin);
			
		} catch (OwnException e) {
			
			viewable.setData(DataAccessible.ERROR, e.toString());
		}
	}
}
