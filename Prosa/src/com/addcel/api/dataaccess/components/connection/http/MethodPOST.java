package com.addcel.api.dataaccess.components.connection.http;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;

import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.components.connection.Connectable;


import com.addcel.api.addcelexception.Error;
import com.addcel.prosa.Start;


import net.rim.blackberry.api.browser.URLEncodedPostData;
import net.rim.device.api.io.http.HttpProtocolConstants;


/*
	Some other notes on POST requests:
	
	POST requests are never cached
	POST requests do not remain in the browser history
	POST requests cannot be bookmarked
	POST requests have no restrictions on data length
*/


public class MethodPOST implements Connectable {

	private HttpConnection httpConnection;
	private InputStream inputStream;
	
	private String data = null;
	private String url = null;
	
	boolean error = false;
	StringBuffer stringBuffer = null;
	
	public MethodPOST(String url){
		this.url = url;
	}
	

	public String createURL() {
		
		String newURL = url + ";" + Start.IDEAL_CONNECTION + ";" + "ConnectionTimeout=30000";
		
		return newURL;
	}
	
	private void sendData() throws OwnException{
		
		try{
			
			stringBuffer = new StringBuffer();

			URLEncodedPostData postData = new URLEncodedPostData(URLEncodedPostData.DEFAULT_CHARSET, false);
			postData.append("json", data);
			
			byte[] postBytes = postData.getBytes();
			
			httpConnection = (HttpConnection) Connector.open(createURL(), Connector.READ_WRITE, true);
			httpConnection.setRequestMethod(HttpConnection.POST);
			httpConnection.setRequestProperty(HttpProtocolConstants.HEADER_CONTENT_LENGTH, Integer.toString(postBytes.length));
			httpConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

			OutputStream strmOut = httpConnection.openOutputStream();
			strmOut.write(postBytes);
			strmOut.flush();
			strmOut.close();
			
		} catch(IOException oie){
			error = true;
			throw new OwnException(Error.HTTP_SEND_DATA);
		}
	}
	
	
	private void getInfo() throws OwnException{
		
		try {
			
			inputStream = httpConnection.openInputStream();
			int c;
			while ((c = inputStream.read()) != -1) {

				stringBuffer.append((char) c);
			}

			System.out.println(stringBuffer.toString());

		}  catch(IOException oie){
			error = true;
			throw new OwnException(Error.HTTP_RECEIVE_DATA);
		} 
	}
	
	
	public void execute(String data) throws OwnException {

		try{

			//doSend(url, data);

			this.data = data;
			
			sendData();			
			
			if (!error){
				
				getInfo();
			}


		} finally {

			try {
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				if (httpConnection != null) {
					httpConnection.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public Object getData() {
		return stringBuffer.toString();
	}
	
	
	
	
	private void doSend(String url, String post){
		int code = 0;
		DataInputStream in = null;
		OutputStream dos;
		String errorStr = null;
		boolean wasError = false;
		ByteArrayOutputStream responseBytes = null;
		HttpConnection connection = null;
		byte data[] = null;
		byte postByte[] = null;
		url = createURL();
		//post = this.data;


		try {

			connection = (HttpConnection) Connector.open(url, Connector.READ_WRITE, true);
			
			
			URLEncodedPostData postData = new URLEncodedPostData(URLEncodedPostData.DEFAULT_CHARSET, false);
			postData.append("json", post);
			
			System.out.println("url : " + url);
			System.out.println("post: " + post);
			System.out.println("json: " + postData.toString());

			//String postTemp = postData.toString();
			
			//postByte = new byte[post.length()];
			postByte = postData.getBytes();
			connection.setRequestMethod(HttpConnection.POST);
			connection.setRequestProperty("content-length", String.valueOf(postByte.length));
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			
			dos = connection.openOutputStream();
			dos.write(postByte);
			dos.flush();
			dos.close();

			in = connection.openDataInputStream();

			int length = (int) connection.getLength();

			if (length != -1) {
				data = new byte[length];

				// Read the png into an array
				in.readFully(data);
			} else {
				responseBytes = new ByteArrayOutputStream();
				int ch;
				while ((ch = in.read()) != -1)
					responseBytes.write(ch);

				data = responseBytes.toByteArray();
			}
			
			String buffer = new String(data);
			
			stringBuffer = new StringBuffer();
			
			stringBuffer.append(buffer);
			
		} catch (Exception e) {
			wasError = true;
			e.printStackTrace();

		}

		finally {

			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

			try {
				if (connection != null) {
					connection.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
