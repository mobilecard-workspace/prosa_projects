package com.addcel.prosa.model.rolestiendas;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.addcelexception.Error;
import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.components.toObject.Objectable;
import com.addcel.prosa.dto.Tienda;

public class OTiendas implements Objectable{

	Vector vector = null;
	
	public Object execute(String data) throws OwnException {
		try {
			
			JSONArray jsonArray = new JSONArray(data);

			int length = jsonArray.length();
			
			vector = new Vector();
			
			for(int index = 0; index < length; index++){
				
				JSONObject jsonObject = (JSONObject)jsonArray.get(index);
				
				Tienda tienda = new Tienda();

				tienda.setIdTienda(jsonObject.optInt("idTienda", 0));
				tienda.setIdProveedor(jsonObject.optInt("idProveedor", 0));
				tienda.setDescripcion(jsonObject.optString("descripcion", null));
				
				vector.addElement(tienda);
			}
			
			return vector;
			
		} catch (JSONException e) {
			e.printStackTrace();
			throw new OwnException(Error.JSON_EXCEPTION);
		}	
	}
}
