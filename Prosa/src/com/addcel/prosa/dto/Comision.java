package com.addcel.prosa.dto;

public class Comision {

	private double comision;
	private double comisionPorcentaje;
	private double minComPorcentaje;
	private double minCom;
	
	public double getComision() {
		return comision;
	}
	public void setComision(double comision) {
		this.comision = comision;
	}
	public double getComisionPorcentaje() {
		return comisionPorcentaje;
	}
	public void setComisionPorcentaje(double comisionPorcentaje) {
		this.comisionPorcentaje = comisionPorcentaje;
	}
	public double getMinComPorcentaje() {
		return minComPorcentaje;
	}
	public void setMinComPorcentaje(double minComPorcentaje) {
		this.minComPorcentaje = minComPorcentaje;
	}
	public double getMinCom() {
		return minCom;
	}
	public void setMinCom(double minCom) {
		this.minCom = minCom;
	}
}
