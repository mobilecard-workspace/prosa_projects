package com.addcel.api.ui.uicomponents.buttons;

import java.util.Vector;


import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.container.HorizontalFieldManager;

public class CustomButtonFieldManager extends HorizontalFieldManager implements FieldManagerObserver{


	private Vector buttons;
	private int width;
	private int preferredWidth;

	public void add(Field customButton){
		
		super.add(customButton);
		
		CustomSelectedSizeButton button = (CustomSelectedSizeButton)customButton;
		
		button.setManagerObserver(this);
		if (buttons == null){
			buttons = new Vector();
		}
		
		buttons.addElement(button);
	}
	

	public CustomButtonFieldManager(){
		
		super();
		width = Display.getWidth();
		//preferredWidth = width/2;
	}
	
	public int getPreferredWidth() {
		return width;
	}


	public void notify(CustomSelectedSizeButton button) {
		
		if (buttons != null){
			
			int size = buttons.size();
			
			for (int index = 0; index < size; index++){
				
				CustomSelectedSizeButton button2 = (CustomSelectedSizeButton)buttons.elementAt(index);
				button2.setSelected(false);
			}
			
			button.setSelected(true);
		}
		
	}
}
