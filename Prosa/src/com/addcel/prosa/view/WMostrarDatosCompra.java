package com.addcel.prosa.view;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.MainScreen;

import com.addcel.api.ui.Viewable;
import com.addcel.api.ui.uicomponents.AnuncioLabelField;
import com.addcel.api.ui.uicomponents.ElementLabelField;
import com.addcel.api.ui.uicomponents.buttons.CustomButtonFieldManager;
import com.addcel.api.ui.uicomponents.buttons.CustomSelectedSizeButton;
import com.addcel.prosa.dto.PagoVisa;

public class WMostrarDatosCompra extends Viewable implements
		FieldChangeListener {

	private ElementLabelField mensajeErrorTxt;
	private ElementLabelField referenciaTxt;
	private ElementLabelField autorizacionTxt;

	private AnuncioLabelField mensajeErrorEdit;
	private AnuncioLabelField referenciaEdit;
	private AnuncioLabelField autorizacionEdit;

	private CustomSelectedSizeButton finalizar;
	private CustomSelectedSizeButton regresar;
	
	private MainScreen mainScreen;

	public WMostrarDatosCompra(boolean isSetTitle, String title,
			PagoVisa pagoVisa, MainScreen mainScreen) {
		super(isSetTitle, title);

		this.mainScreen = mainScreen;
		
		mensajeErrorTxt = new ElementLabelField("Mensaje");
		referenciaTxt = new ElementLabelField("Folio");
		autorizacionTxt = new ElementLabelField("Autorización");

		mensajeErrorEdit = new AnuncioLabelField(pagoVisa.getMensajeError());
		referenciaEdit = new AnuncioLabelField(pagoVisa.getReferencia());
		autorizacionEdit = new AnuncioLabelField(pagoVisa.getAutorizacion());

		add(new NullField());
		add(mensajeErrorTxt);
		add(mensajeErrorEdit);
		add(new LabelField());
		add(referenciaTxt);
		add(referenciaEdit);
		add(new LabelField());
		add(autorizacionTxt);
		add(autorizacionEdit);
		add(new LabelField());
		
		
		if (pagoVisa.getIdError() == 0){
			
			finalizar = new CustomSelectedSizeButton("Finalizar", 1);
			finalizar.setChangeListener(this);
			add(finalizar);
		} else {
			
			regresar = new CustomSelectedSizeButton("Reintentar", 2);
			regresar.setChangeListener(this);

			finalizar = new CustomSelectedSizeButton("Finalizar", 2);
			finalizar.setChangeListener(this);
			
			CustomButtonFieldManager buttonFieldManager = new CustomButtonFieldManager();
			buttonFieldManager.add(regresar);
			buttonFieldManager.add(finalizar);
			add(buttonFieldManager);
			
		}
	}

	protected void analyzeData(int request, Object object) {

	}

	public void fieldChanged(Field field, int context) {

		if (field == finalizar) {

			UiApplication.getUiApplication().popScreen(this);
			UiApplication.getUiApplication().popScreen(mainScreen);

		} else if (field == regresar) {

			UiApplication.getUiApplication().popScreen(this);

		}
	}

}
