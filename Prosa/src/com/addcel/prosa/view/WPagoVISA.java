package com.addcel.prosa.view;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BasicEditField;
import net.rim.device.api.ui.component.Dialog;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.addcelexception.Error;
import com.addcel.api.dataaccess.DataAccessible;
import com.addcel.api.ui.Viewable;
import com.addcel.api.ui.uicomponents.CustomEditField;
import com.addcel.api.ui.uicomponents.ElementLabelField;
import com.addcel.api.ui.uicomponents.buttons.CustomButtonFieldManager;
import com.addcel.api.ui.uicomponents.buttons.CustomSelectedSizeButton;
import com.addcel.prosa.Start;
import com.addcel.prosa.dto.Comision;
import com.addcel.prosa.dto.PagoVisa;
import com.addcel.prosa.model.pagovisa.DAPagoVISA;


public class WPagoVISA extends Viewable implements FieldChangeListener{

	
	private ElementLabelField nombreTxt;
	private ElementLabelField mailTxt;
	private ElementLabelField descripcionProductoTxt;
	private ElementLabelField numTarjetaTxt;
	private ElementLabelField vigenciaTxt;
	private ElementLabelField cvv2Txt;
	private ElementLabelField montoTxt;
	private ElementLabelField comisionTxt;
	private ElementLabelField totalTxt;
	
	private CustomEditField nombreEdit;
	private CustomEditField mailEdit;
	private CustomEditField descripcionProductoEdit;
	private CustomEditField numTarjetaEdit;
	private CustomEditField vigenciaEdit;
	private CustomEditField cvv2Edit;
	private CustomEditField montoEdit;
	private CustomEditField comisionEdit;
	private CustomEditField totalEdit;

	private CustomSelectedSizeButton pesos;
	private CustomSelectedSizeButton dolares;
	
	private CustomSelectedSizeButton pagar;	

	private int tipoMoneda = 1;
	
	public WPagoVISA(boolean isSetTitle, String title) {
		super(isSetTitle, title);

		nombreTxt = new ElementLabelField("Nombre");
		mailTxt = new ElementLabelField("Email");
		descripcionProductoTxt = new ElementLabelField("Descripci�n del producto");
		numTarjetaTxt = new ElementLabelField("N�mero de tarjeta");
		vigenciaTxt = new ElementLabelField("Vigencia");
		cvv2Txt = new ElementLabelField("CVV2");
		montoTxt = new ElementLabelField("Monto(MXN)");
		comisionTxt = new ElementLabelField("Comisi�n(MXN)");
		totalTxt = new ElementLabelField("Total(MXN)");

		nombreEdit = new CustomEditField("juan");
		mailEdit = new CustomEditField("alberto.solis@addcel.com");
		descripcionProductoEdit = new CustomEditField(150, Field.EDITABLE);
		descripcionProductoEdit.setText("teclado");
		numTarjetaEdit = new CustomEditField("1234567890123456");
		vigenciaEdit = new CustomEditField("05/16");
		cvv2Edit = new CustomEditField("000");
		montoEdit = new CustomEditField("100");
		montoEdit.setChangeListener(this);
		comisionEdit = new CustomEditField(20, Field.NON_FOCUSABLE);
		totalEdit = new CustomEditField(20, Field.NON_FOCUSABLE);

		pesos = new CustomSelectedSizeButton("Pesos", 2);
		pesos.setSelected(true);
		dolares = new CustomSelectedSizeButton("D�lares", 2);
		pesos.setChangeListener(this);
		dolares.setChangeListener(this);

		pagar = new CustomSelectedSizeButton("Pagar", 1);
		pagar.setChangeListener(this);

		add(nombreTxt);
		add(nombreEdit);
		add(mailTxt);
		add(mailEdit);
		add(descripcionProductoTxt);
		add(descripcionProductoEdit);
		add(numTarjetaTxt);
		add(numTarjetaEdit);
		add(vigenciaTxt);
		add(vigenciaEdit);
		add(cvv2Txt);
		add(cvv2Edit);

		add(montoTxt);
		add(montoEdit);
		
		
		CustomButtonFieldManager fieldManager = new CustomButtonFieldManager();
		fieldManager.add(pesos);
		fieldManager.add(dolares);
		
		
		add(fieldManager);
		add(comisionTxt);
		add(comisionEdit);
		add(totalTxt);
		add(totalEdit);

		add(pagar);
	}


	
	public void fieldChanged(Field field, int context) {

		if(field == pagar){

			if (checarCampos()){
				
				JSONObject jsonObject = new JSONObject();
				
				try {
					jsonObject.put("tarjeta", numTarjetaEdit.getText());
					jsonObject.put("cvv2", cvv2Edit.getText());
					jsonObject.put("vigencia", vigenciaEdit.getText());
		   
		   			jsonObject.put("email", mailEdit.getText());
					jsonObject.put("nombres", nombreEdit.getText());
					jsonObject.put("descProducto", descripcionProductoEdit.getText());
					jsonObject.put("subTotal", montoEdit.getText());
					jsonObject.put("total", totalEdit.getText());
					jsonObject.put("moneda", tipoMoneda);
					
					jsonObject.put("tipo", 1); //tarjeta
					jsonObject.put("idUser", Start.respuestaLogin.getIdUser());
					jsonObject.put("idProveedor", 20);
					
					Thread thread = new Thread(new DAPagoVISA(this, jsonObject));
					thread.start();
					
				} catch (JSONException e) {
					e.printStackTrace();
					Dialog.alert(Error.JSON_EXCEPTION);
				}
				
			} else {
				
				Dialog.alert(Error.EDITFIELDS_NO_COMPLETADOS);
			}

		} else if(field == pesos){
			tipoMoneda = 1;
		} else if(field == dolares){
			tipoMoneda = 2;
		} else if(field == montoEdit){
			
			String sMonto = montoEdit.getText();
			
			double monto = Double.parseDouble(sMonto);
			double total = 0;
			double showComision = 0;
			double tipoCambio = Start.respuestaLogin.getTipoCambio();
			
			Comision comisiones = Start.respuestaLogin.getComisiones();
			
			 double comision = comisiones.getComision();
			 double comisionPorcentaje = comisiones.getComisionPorcentaje();
			 double minComPorcentaje = comisiones.getMinComPorcentaje();
			 double minCom = comisiones.getMinCom();
			
			if (monto < minComPorcentaje){
				//showComision = tipoMoneda == 2 ? comision/tipoCambio : comision;
				
				showComision = comision;
				total = monto + showComision;
				
			} else {
				
				total = monto + (monto * comisionPorcentaje);
				showComision = (monto * comisionPorcentaje);
			}
			
			comisionEdit.setText(String.valueOf(showComision));
			totalEdit.setText(String.valueOf(total));
			
		}
	}

	protected void analyzeData(int request, Object object) {

		if (request == DataAccessible.ERROR){
			
			String message = cleanMessageError(object);
			Dialog.alert(message);
			
		} else if (request == DataAccessible.DATA){
			
			if (object instanceof PagoVisa){
				
				//UiApplication.getUiApplication().popScreen(this);
				UiApplication.getUiApplication().pushScreen(new WMostrarDatosCompra(true, "Resumen de compra", (PagoVisa)object, this));
			}
		}
	}
	
	
	
	private boolean checarCampos(){
		
		boolean check = true;
		
		if (invalidText(nombreEdit)){
			check = false;
		} else if (invalidText(nombreEdit)){
			check = false;
		} else if (invalidText(mailEdit)){
			check = false;
		} else if (invalidText(descripcionProductoEdit)){
			check = false;
		} else if (invalidText(numTarjetaEdit)){
			check = false;
		} else if (invalidText(vigenciaEdit)){
			check = false;
		} else if (invalidText(nombreEdit)){
			check = false;
		} else if (invalidText(cvv2Edit)){
			check = false;
		} else if (invalidText(montoEdit)){
			check = false;
		} else if (invalidText(comisionEdit)){
			check = false;
		} else if (invalidText(totalEdit)){
			check = false;
		}
		
		return check;
	}
	
	
	private boolean invalidText(BasicEditField editField){
		
		String temp = editField.getText();
		
		if ((temp != null)&&(temp.length()>0)){
			return false;
		} else {
			return true;
		}
	}
	
	
}
