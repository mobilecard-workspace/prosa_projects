package com.addcel.prosa.controller;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.addcel.prosa.services.ProsaPagoAMEX;
import com.addcel.prosa.services.ProsaPagosService;
import com.addcel.prosa.services.ProsaService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class ProsaController {
	
	private static final Logger logger = LoggerFactory.getLogger(ProsaController.class);
	@Autowired
	private ProsaService dfService;
	@Autowired
	private ProsaPagosService dfpService;
	@Autowired
	private ProsaPagoAMEX amexService;
	
	@RequestMapping(value = "/", produces = { "application/json;charset=UTF-8" })
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}. á é í óp ñ Ñ ", locale);				
		return "home";
	}
	
	@RequestMapping(value = "/addUser", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String addUser(@RequestParam("json")String json) {	
		logger.info("Dentro del servicio: ProsaServicios/addUser.........................");
		return dfService.addUser(json);
	}
	
	@RequestMapping(value = "/login", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String login(@RequestParam("json")String json) {		
		logger.info("Dentro del servicio: ProsaServicios/login..........................");
		return dfService.login(json);
	}
	
	@RequestMapping(value = "/updateUser", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String updateUser(@RequestParam("json")String json) {
		logger.info("Dentro del servicio: ProsaServicios/updateUser.......................");
		return dfService.updateUser(json);
	}
	
	@RequestMapping(value = "/findUser")
	public @ResponseBody String findUser(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: ProsaServicios/findUser.........................");
		return dfService.findUser(data);
	}
	
	@RequestMapping(value = "/deleteUser", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String deleteUser(@RequestParam("json") String jsonEnc) {
		logger.info("Dentro del servicio: ProsaServicios/deleteUser.........................");
		return dfService.deleteUser(jsonEnc);	
	}
	
	@RequestMapping(value = "/getTiendas", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String listaTiendas(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: ProsaServicios/getTiendas.........................");
		return dfService.listaTiendas(data);
	}
	
	@RequestMapping(value = "/getRoles", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String listaRoles(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: ProsaServicios/getRoles.........................");
		return dfService.listaRoles(data);
	}
	
	@RequestMapping(value = "/getVendedor", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String getVendor(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: ProsaServicios/getVendedor.........................");
		return dfService.getVendor(data);
	}
	
	@RequestMapping(value = "/getToken", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String getToken(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: ProsaServicios/getToken.........................");
		return dfService.generaToken(data);
	}
	
	/**
	 * Se utiliza para los roles supervisor y vendedor
	 * @param jsonEnc
	 * @return string encriptado con la respuesta del servicio
	 */
	@RequestMapping(value = "/updatePass", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String actualizarPassword(@RequestParam("json") String jsonEnc) {		
		logger.info("Dentro del servicio: ProsaServicios/updatePass.........................");
		return dfService.actualizaPassword(jsonEnc,0);
	}
	
	@RequestMapping(value = "/updatePassStatus", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String actualizarPasswordStatus(@RequestParam("json") String jsonEnc) {		
		logger.info("Dentro del servicio: ProsaServicios/updatePassStatus.........................");
		return dfService.actualizaPassword(jsonEnc,1);
	}
	
	@RequestMapping(value = "/pago-visa", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String  datosPago(@RequestParam("json") String jsonEnc) {	
		logger.info("Dentro del servicio: ProsaServicios/pago-visa.........................");
		return dfpService.procesaPago(jsonEnc);	
	}
	
	@RequestMapping(value = "/pago-amex", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String pagoAmex(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: ProsaServicios/pago-amex.........................");
		return amexService.procesaPago(data);
	}
	
	@RequestMapping(value = "/busquedaPagos", method=RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String busquedaPagos(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: ProsaServicios/busquedaPagos.........................");
		return dfService.busquedaPagos(data);
	}
}