package com.addcel.prosa.model.modificarcontrasenia;

import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.DataAccessible;
import com.addcel.api.dataaccess.components.connection.http.MethodPOST;
import com.addcel.api.ui.Viewable;
import com.addcel.prosa.model.URL;
import com.addcel.prosa.model.user.OContestacion;

public class DAModificarContrasenia extends DataAccessible implements Runnable {

	public DAModificarContrasenia(Viewable viewable, String data) {
		super(viewable, data);
	}

	public void run() {
		execute(viewable, data);
	}

	protected void execute(Viewable viewable, String json) {

		try {

			String encrypt = this.encrypt(CRYPTO_HARD, json);

			connectable = new MethodPOST(URL.URL_CAMBIAR_CONTRASENIA);
			connectable.execute(encrypt);

			String httpAnswer = (String)connectable.getData();

			String decrypt = this.decrypt(CRYPTO_HARD, httpAnswer);

			OContestacion contestacion = new OContestacion();
			Object alta = contestacion.execute(decrypt);
			viewable.setData(DataAccessible.DATA, alta);

		} catch (OwnException e) {
			viewable.setData(DataAccessible.ERROR, e.toString());
		}
	}
}
