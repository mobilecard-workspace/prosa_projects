package com.addcel.prosa.view;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BasicEditField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.addcelexception.Error;
import com.addcel.api.dataaccess.DataAccessible;
import com.addcel.api.ui.Viewable;
import com.addcel.api.ui.uicomponents.CustomPasswordEditField;
import com.addcel.api.ui.uicomponents.ElementLabelField;
import com.addcel.api.ui.uicomponents.buttons.CustomSelectedButtonField;
import com.addcel.prosa.Start;
import com.addcel.prosa.dto.ContestacionAlta;
import com.addcel.prosa.dto.RespuestaLogin;
import com.addcel.prosa.model.userestatus.DAUserEstatus;


public class WModificarNuevaContrasenia extends Viewable implements FieldChangeListener{

	private CustomPasswordEditField passEdit01;
	private CustomPasswordEditField passEdit02;
	
	private ElementLabelField passTxt01;
	private ElementLabelField passTxt02;
	
	private CustomSelectedButtonField aceptar;
	private CustomSelectedButtonField cancelar;

	private RespuestaLogin respuestaLogin;
	
	private String login;
	
	public WModificarNuevaContrasenia(boolean isSetTitle, String title, RespuestaLogin respuestaLogin, String login) {
		super(isSetTitle, title);

		this.respuestaLogin = respuestaLogin;
		this.login = login;
		
		passEdit01 = new CustomPasswordEditField();
		passEdit02 = new CustomPasswordEditField();

		passTxt01 = new ElementLabelField("Nueva contrase�a");
		passTxt02 = new ElementLabelField("Confirmar nueva contrase�a");
		
		aceptar = new CustomSelectedButtonField("Aceptar", ButtonField.CONSUME_CLICK);
		cancelar = new CustomSelectedButtonField("Cancelar", ButtonField.CONSUME_CLICK);

		aceptar.setChangeListener(this);
		cancelar.setChangeListener(this);
		
		HorizontalFieldManager fieldManager = new HorizontalFieldManager();
		
		add(passTxt01);
		add(passEdit01);
		add(passTxt02);
		add(passEdit02);
		add(new LabelField(""));
		fieldManager.add(cancelar);
		fieldManager.add(aceptar);
		add(fieldManager);
	}

	protected void analyzeData(int request, Object object) {
		
		if (request == DataAccessible.ERROR){
			
			String message = cleanMessageError(object);
			Dialog.alert(message);
		} else {
			
			ContestacionAlta contestacionAlta = (ContestacionAlta)object;
			String message = contestacionAlta.getMensajeError();
			Start.respuestaLogin.setPassword(passEdit01.getText());
			Dialog.alert(message);
		}
	}

	public void fieldChanged(Field field, int context) {

		if (field == aceptar){
			
			if (checarCampos()){
				
				String pass01 = passEdit01.getText();
				String pass02 = passEdit02.getText();
				
				if (pass01.equals(pass02)){

					try {
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("idUser", respuestaLogin.getIdUser());
						jsonObject.put("idProveedor", 20);
						jsonObject.put("idSupervisor", respuestaLogin.getIdUser());
						jsonObject.put("login", login);
						jsonObject.put("password", pass01);
						
						Thread thread = new Thread(new DAUserEstatus(this, jsonObject.toString()));
						thread.start();
						
					} catch (JSONException e) {
						e.printStackTrace();
						Dialog.alert(Error.JSON_EXCEPTION);
					}
					
				} else {
					
					Dialog.alert(Error.PASSWORDS_IGUALES);
				}
				
				
			} else {
				Dialog.alert("Verificar campos, el tama�o deber� ser entre 8 y 12 caracteres.");
			}
			
		} else if(field == cancelar){
			
			UiApplication.getUiApplication().pushScreen(this);
		}
	}
	
	
	private boolean checarCampos(){
		
		boolean check = true;
		
		if (invalidText(passEdit01)){
			check = false;
		} else if (invalidText(passEdit02)){
			check = false;
		}
		
		return check;
	}
	

	private boolean invalidText(BasicEditField editField){
		
		String temp = editField.getText();
		
		if ((temp != null)&&(temp.length()>=8)&&(temp.length()<=12)){
			return false;
		} else {
			return true;
		}
	}

}
