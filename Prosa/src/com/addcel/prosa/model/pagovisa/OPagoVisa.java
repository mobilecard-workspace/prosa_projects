package com.addcel.prosa.model.pagovisa;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.addcelexception.Error;
import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.components.toObject.Objectable;
import com.addcel.prosa.dto.PagoVisa;

public class OPagoVisa implements Objectable{

	public Object execute(String data) throws OwnException {

		JSONObject jsonObject;
		
		try {
			
			jsonObject = new JSONObject(data);
			
			PagoVisa pagoVisa = new PagoVisa();
			
			pagoVisa.setIdError(jsonObject.optInt("idError", 0));
			pagoVisa.setMensajeError(jsonObject.optString("mensajeError", null));
			pagoVisa.setAutorizacion(jsonObject.optString("referencia", null));
			pagoVisa.setReferencia(jsonObject.optString("autorizacion", null));
			
			return pagoVisa;
			
		} catch (JSONException e) {
			e.printStackTrace();
			throw new OwnException(Error.JSON_EXCEPTION);
		}
	}
}
