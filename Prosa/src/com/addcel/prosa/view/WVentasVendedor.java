package com.addcel.prosa.view;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.DateField;

import com.addcel.api.ui.UtilColor;
import com.addcel.api.ui.Viewable;
import com.addcel.api.ui.uicomponents.ElementLabelField;
import com.addcel.api.ui.uicomponents.buttons.CustomSelectedSizeButton;



public class WVentasVendedor extends Viewable implements FieldChangeListener{

	
	private ElementLabelField inicioTxt;
	private ElementLabelField corteTxt;
	
	private DateField inicio;
	private DateField corte;
	
	private CustomSelectedSizeButton consultar;
	
	public WVentasVendedor(boolean isSetTitle, String title) {
		super(isSetTitle, title);
		
		inicio = new DateField("", System.currentTimeMillis(),
				DateField.DATE) {
			protected void paint(Graphics graphics) {
				graphics.setColor(UtilColor.ELEMENT_STRING);
				super.paint(graphics);
			}
		};
		
		
		corte = new DateField("", System.currentTimeMillis(),
				DateField.DATE) {
			protected void paint(Graphics graphics) {
				graphics.setColor(UtilColor.ELEMENT_STRING);
				super.paint(graphics);
			}
		};
		
		inicioTxt = new ElementLabelField("Fecha de inicio");
		corteTxt = new ElementLabelField("Fecha de corte");
		
		consultar = new CustomSelectedSizeButton("Consultar", 1);
		
		add(inicioTxt);
		add(inicio);

		add(corteTxt);
		add(corte);
		
		add(consultar);
	}

	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
		
	}

	protected void analyzeData(int request, Object object) {
		// TODO Auto-generated method stub
		
	}

}
