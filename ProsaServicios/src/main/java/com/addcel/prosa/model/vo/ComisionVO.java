package com.addcel.prosa.model.vo;

public class ComisionVO {
	private Double comision;
	private Double comisionPorcentaje;
	private Double minComPorcentaje;
	private Double minCom;
	
	public Double getComision() {
		return comision;
	}
	public void setComision(Double comision) {
		this.comision = comision;
	}
	public Double getComisionPorcentaje() {
		return comisionPorcentaje;
	}
	public void setComisionPorcentaje(Double comisionPorcentaje) {
		this.comisionPorcentaje = comisionPorcentaje;
	}
	public Double getMinComPorcentaje() {
		return minComPorcentaje;
	}
	public void setMinComPorcentaje(Double minComPorcentaje) {
		this.minComPorcentaje = minComPorcentaje;
	}
	public Double getMinCom() {
		return minCom;
	}
	public void setMinCom(Double minCom) {
		this.minCom = minCom;
	}
	
}
