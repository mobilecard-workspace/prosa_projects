package com.addcel.api.dataaccess;

import java.util.Date;

import org.json.me.JSONObject;

import com.addcel.api.dataaccess.components.connection.Connectable;
import com.addcel.api.dataaccess.components.descryption.Descryptionable;
import com.addcel.api.dataaccess.components.encryption.Encryptionable;
import com.addcel.api.dataaccess.components.toJson.Jsonable;
import com.addcel.api.dataaccess.components.toObject.Objectable;
import com.addcel.api.ui.Viewable;
import com.addcel.api.util.security.AddcelCrypto;


public abstract class DataAccessible {
	
	public final static int ERROR = 0;
	public final static int DATA = 1;
	
	public final static int CRYPTO_HARD = 0;
	public final static int CRYPTO_SENS = 1;
	
	protected com.addcel.api.ui.Viewable viewable;
	protected String data;
	
	protected Connectable connectable;
	protected Descryptionable descryptionable;
	protected Encryptionable encryptionable;
	protected Jsonable jsonable;
	protected Objectable objectable;
	
	protected JSONObject jsonObject;
	
	public DataAccessible(Viewable viewable, String data){
		this.viewable = viewable;
		this.data = data;
	}
	
	public DataAccessible(Viewable viewable, JSONObject jsonObject){
		this.viewable = viewable;
		this.jsonObject = jsonObject;
	}
	
	protected String decrypt(int type, String json) {

		String encrypTemp = null;
		
		switch (type) {
		case CRYPTO_HARD:
			encrypTemp = AddcelCrypto.decryptHard(json);
			break;
			
		case CRYPTO_SENS:
			encrypTemp = AddcelCrypto.decryptSensitive(json);
			break;
		}
		
		return encrypTemp;
	}
	
	
	protected String encrypt(int type, String json) {

		String encrypTemp = null;
		
		switch (type) {
		case CRYPTO_HARD:
			encrypTemp = AddcelCrypto.encryptHard(json);
			break;
			
		case CRYPTO_SENS:
			String key = getKey();
			encrypTemp = AddcelCrypto.encryptSensitive(key, json);
			break;
		}
		
		return encrypTemp;
	}
	
	
	protected String getKey(){
		
		Date hoy = new Date();
		long lkey = hoy.getTime();
		String skey = String.valueOf(lkey);
		
		int length = skey.length();
		
		skey = skey.substring(length - 8, length);
		
		return skey;
	}
	
	
	abstract protected void execute(Viewable viewable, String json);
}
