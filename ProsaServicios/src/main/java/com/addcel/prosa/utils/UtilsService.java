package com.addcel.prosa.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UtilsService {
	private static final Logger logger = LoggerFactory.getLogger(UtilsService.class);
	private static final String body = 
			"<html><head> <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">  <style type=\"text/css\">  body { background: none repeat scroll 0 0 #FFFFFF; font-family: 'Open Sans', sans-serif;  } table.info_description{ height: auto; width: 400px; position: relative; } table.info_description td{ height: auto; color: #DF1D44; font-size: 20px; line-height: 32px; text-align: left; display: inline-block; vertical-align: top; *display: inline; *zoom:1; } section.main_info{ background: none repeat scroll 0 0 #CBCBCB; width: 100%; height: auto; margin: 0 auto; background:#CBCBCB; }  table.donate{ width: 400px; }  table.donate p.text_donation_centers{ color: #757575; font-size: 14px; font-style:italic; line-height: 20px; text-align: left; } table.donate p.text_donation_centers span.highlights{ font-weight: bold; }  table.block{ width: 400px; height: auto; color: #232323; } table.block td.title_rigth{ font-weight: normal; text-transform:uppercase; font-size: 10px; line-height: 16px; text-align: right; width: 50px; } table.block td.address_center{ margin: 0 0 18px; font-weight: bold; font-style:italic; font-size: 12px; line-height: 14px; text-align: left; }  section.helpers{ background: none repeat scroll 0 0 #FFFFFF; width: 400px; height: auto; position: relative; padding: 26px 0 0; margin: 0 auto; vertical-align: center; color: #000000; font-weight: bold; text-transform:uppercase; font-size: 10px; line-height: 14px; text-align: left; } section.helpers div.extra_text_helpers{ margin: 44px auto 20px;  } section.helpers div.footer_text_helpers{ margin: 0 0 0 0;  } </style> </head> <body data-twttr-rendered=\"true\">  <table class=\"info_description\"  align=\"center\"> <tbody> <tr> <td><img src=\"cid:identifierCID00\"></td> </tr> <tr> <td>¡Una empresa de clase mundial!</td> </tr> </tbody> </table>  <section class=\"main_info\">  <table class=\"donate\"  align=\"center\"> <tbody> <tr> <td >&nbsp </td> </tr> <tr> <td > <p class=\"text_donation_centers\"> Estimado/a <span class=\"highlights\"><#NOMBRES#></span>. </p> </td> </tr> <tr> <td > <p class=\"text_donation_centers\"> ¡Le damos la bienvenida! Ahora está registrado como usuario de <span class=\"highlights\">PROSA MOVIL</span>. Sus datos personales para iniciar sesión son: </p> </td> </tr> </tbody> </table>  <table class=\"block\"  align=\"center\" cellpadding=\"2\"> <tbody> <tr> <td class=\"title_rigth\"> Login: </td> <td class=\"address_center\"> <#LOGIN#> </td> <td class=\"address_center\"> </td> </tr> <tr> <td class=\"title_rigth\"> Password: </td> <td class=\"address_center\"> <#PASSWD#> </td> <td class=\"address_center\"> </td> </tr> <tr> <td >&nbsp </td> <td >&nbsp </td> <td >&nbsp </td> </tr> </tbody> <table class=\"donate\"  align=\"center\"> <tbody> <tr> <td >&nbsp </td> </tr> <tr> <td > <p class=\"text_donation_centers\"> Cordialmente,<br> El equipo de <span class=\"highlights\">PROSA MOVIL</span>. </p> </td> </tr> </tbody> </table> </table> </section>  <section class=\"helpers\"> <div class=\"extra_text_helpers\"> Nota. Este correo es de caráter informativo, no es necesario que responda al mismo. </div> <div class=\"footer_text_helpers\"> By <img src=\"cid:identifierCID01\"> </div> </section>  </body> </html>";
	@Autowired
	private ObjectMapper mapperJk;	
	
	public <T> String objectToJson(T object){
		String json=null;
		try {
			json = mapperJk.writeValueAsString(object);
//			logger.info("Encoding DEFAULT: {}",json);
			json = new String(json.getBytes(),"UTF-8");
//			logger.info("Encoding ISO-8859-1: {}",json);
		} catch (JsonGenerationException e) {
			logger.error("1.- Error al generar json: {}",e);
		} catch (JsonMappingException e) {
			logger.error("2.- Error al mapperar objeccto: {}",e);
		} catch (IOException e) {
			logger.error("3.- Error al generar json: {}",e);
		}
		return json;
	}

	public <T> Object jsonToObject(String json, Class<T> clase) {		
		Object obj = null;
		try {
			obj = mapperJk.readValue(json, clase);
		} catch (JsonParseException e) {
			logger.error("Error al parsear cadena json 1: {}", e);
		} catch (JsonMappingException e) {
			logger.error("Error al parsear cadena json 2: {}", e);
		} catch (IOException e) {
			logger.error("Error al parsear cadena json 3: {}", e);
		}
		return obj;
	}
	
	public String peticionUrlPostParams(String urlRemota,String parametros){
		String respuesta = null;
		//http://50.57.192.210:8080/Conversor/currency?fromCurrency=usd&toCurrency=MXN
		URL url;
		try {
			url = new URL(urlRemota);		
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		// Se indica que se escribira en la conexi�n
		con.setDoOutput(true);
		con.setRequestMethod("POST");
		// Se escribe los parametros enviados a la url
		OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());		
		wr.write(parametros);
		wr.close();			
		if(con.getResponseCode() == HttpURLConnection.HTTP_OK ||
				con.getResponseCode() == HttpURLConnection.HTTP_ACCEPTED){			
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
			StringBuilder sb = new StringBuilder();
			String jsonString;
			while ((jsonString = br.readLine()) != null) {
				sb.append(jsonString);
			}
			respuesta=sb.toString();			
		}else{
			respuesta = "ERROR";
		}
		con.disconnect();
		} catch (MalformedURLException e) {
			logger.error("ERROR URL malformada: {}",e);
		} catch (IOException e) {
			logger.error("ERROR IO : {}",e);
		}
		return respuesta;
	}
}
