package com.addcel.prosa.view;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.MenuItem;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

import com.addcel.api.ui.Viewable;
import com.addcel.api.ui.uicomponents.CustomEditField;
import com.addcel.api.ui.uicomponents.ElementLabelField;
import com.addcel.api.ui.uicomponents.buttons.CustomSelectedButtonField;

public class WBusqueda extends Viewable implements FieldChangeListener{

	private ElementLabelField nombreTxt;
	private ElementLabelField apellidoPaternoTxt;
	private ElementLabelField apellidoMaternoTxt;
	private ElementLabelField loginTxt;
	
	private CustomEditField nombreEdit;
	private CustomEditField apellidoPaternoEdit;
	private CustomEditField apellidoMaternoEdit;
	private CustomEditField loginEdit;

	private CustomSelectedButtonField buscar;
	private CustomSelectedButtonField regresar;
	
	public WBusqueda(boolean isSetTitle, String title) {
		super(isSetTitle, title);
		
		nombreTxt = new ElementLabelField("Nombre:");
		apellidoPaternoTxt = new ElementLabelField("Apellido paterno:");
		apellidoMaternoTxt = new ElementLabelField("Apellido materno:");
		loginTxt = new ElementLabelField("Login:");
		
		nombreEdit = new CustomEditField();
		apellidoPaternoEdit = new CustomEditField();
		apellidoMaternoEdit = new CustomEditField();
		loginEdit = new CustomEditField();

		buscar = new CustomSelectedButtonField("Buscar", ButtonField.CONSUME_CLICK);
		regresar = new CustomSelectedButtonField("Regresar", ButtonField.CONSUME_CLICK);
		buscar.setChangeListener(this);
		regresar.setChangeListener(this);
		
		add(nombreTxt);
		add(nombreEdit);
		add(apellidoPaternoTxt);
		add(apellidoPaternoEdit);

		add(apellidoMaternoTxt);
		add(apellidoMaternoEdit);
		add(loginTxt);
		add(loginEdit);
		add(new LabelField());

		HorizontalFieldManager fieldManager = new HorizontalFieldManager();
		fieldManager.add(regresar);
		fieldManager.add(buscar);

		add(fieldManager);
		
		createMenu();
		
	}

	
	private void createMenu() {

		MenuItem modificar = new MenuItem("Modificar/Actualizar", 110, 10) {
			public void run() {
				UiApplication.getUiApplication().pushScreen(new WModificarDatos(true, "Modificar/Actualizar"));
			}
		};
		
		
		MenuItem contrasenia = new MenuItem("Modificar contraseņa", 110, 10) {
			public void run() {
				//UiApplication.getUiApplication().pushScreen(new WModificarContrasenia(true, "Modificar contraseņa"));
			}
		};

		addMenuItem(modificar);
		addMenuItem(contrasenia);
	}	
	
	public void fieldChanged(Field field, int context) {

		if (field == buscar){
			UiApplication.getUiApplication().pushScreen(new WListaVendedores(true, "Modificar contraseņa"));
		}
	}

	protected void analyzeData(int request, Object object) {
		// TODO Auto-generated method stub
		
	}
}
