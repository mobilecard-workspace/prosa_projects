package com.addcel.api.ui.uicomponents.buttons;

public interface FieldManagerObserver {

	public void notify(CustomSelectedSizeButton button);
}
