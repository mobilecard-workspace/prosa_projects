package com.addcel.prosa.view;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BasicEditField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.addcelexception.Error;
import com.addcel.api.dataaccess.DataAccessible;
import com.addcel.api.ui.Viewable;
import com.addcel.api.ui.uicomponents.CustomEditField;
import com.addcel.api.ui.uicomponents.CustomPasswordEditField;
import com.addcel.api.ui.uicomponents.ElementLabelField;
import com.addcel.api.ui.uicomponents.buttons.CustomSelectedButtonField;
import com.addcel.prosa.Start;
import com.addcel.prosa.dto.RespuestaLogin;
import com.addcel.prosa.model.login.DALogin;


public class WLogin extends Viewable implements FieldChangeListener{

	private CustomEditField userEdit;
	private CustomPasswordEditField passEdit;
	
	private ElementLabelField userTxt;
	private ElementLabelField passTxt;
	
	private CustomSelectedButtonField aceptar;
	private CustomSelectedButtonField cancelar;
	
	private String login;

	public WLogin(boolean isSetTitle, String title) {
		super(isSetTitle, title);

		userEdit = new CustomEditField("root");
		passEdit = new CustomPasswordEditField("12345678");

		userTxt = new ElementLabelField("Usuario");
		passTxt = new ElementLabelField("Contrase�a");
		
		aceptar = new CustomSelectedButtonField("Iniciar sesi�n", ButtonField.CONSUME_CLICK);
		cancelar = new CustomSelectedButtonField("Cancelar", ButtonField.CONSUME_CLICK);

		aceptar.setChangeListener(this);
		cancelar.setChangeListener(this);
		
		HorizontalFieldManager fieldManager = new HorizontalFieldManager();
		
		add(userTxt);
		add(userEdit);
		add(passTxt);
		add(passEdit);
		add(new LabelField(""));
		fieldManager.add(cancelar);
		fieldManager.add(aceptar);
		add(fieldManager);
	}


	protected void analyzeData(int request, Object object) {
		
		if (request == DataAccessible.ERROR){

			String message = cleanMessageError(object);
			Dialog.alert(message);
			
		} else if (request == DataAccessible.DATA){
			
			if (object instanceof RespuestaLogin){

				RespuestaLogin respuestaLogin = (RespuestaLogin)object;

				if (respuestaLogin.getIdError() == 0){

					respuestaLogin.setUser(userEdit.getText());
					respuestaLogin.setPassword(passEdit.getText());
					
					Start.respuestaLogin = respuestaLogin;
					
					UiApplication.getUiApplication().popScreen(this);

					if (respuestaLogin.getIdRol() == RespuestaLogin.USER_ADMINISTRADOR){
						UiApplication.getUiApplication().pushScreen(new WMenuAdministrador(true, "Opciones"));
					} else if (respuestaLogin.getIdRol() == RespuestaLogin.USER_VENDEDOR){
						UiApplication.getUiApplication().pushScreen(new WMenuVendedor(true, "Opciones"));
					}
					
				} else if (respuestaLogin.getIdError() == 2){

					UiApplication.getUiApplication().pushScreen(new WModificarNuevaContrasenia(true, "Modificar datos", respuestaLogin, login));
				} else {
					
					Dialog.alert(respuestaLogin.getMensajeError());
				}
			}
		}
	}


	public void fieldChanged(Field field, int context) {

		if (field == aceptar){
			
			if (checarCampos()){
				
				JSONObject jsonObject = new JSONObject();

				try {
					login = userEdit.getText();
					
					jsonObject.put("idProveedor", "20");
					jsonObject.put("login", login);
					jsonObject.put("password", passEdit.getText());
					
					Thread thread = new Thread(new DALogin(this, jsonObject.toString()));
					thread.start();
				
				} catch (JSONException e) {
					e.printStackTrace();
					Dialog.alert(Error.JSON_EXCEPTION);
				}
			} else {
				
				Dialog.alert(Error.EDITFIELDS_NO_COMPLETADOS);
			}
			
		} else if (field == cancelar){

			close();
		}
	}


	private boolean checarCampos(){
		
		boolean check = true;
		
		if (invalidText(userEdit)){
			check = false;
		} else if (invalidText(passEdit)){
			check = false;
		}
		
		return check;
	}
	

	private boolean invalidText(BasicEditField editField){
		
		String temp = editField.getText();
		
		if ((temp != null)&&(temp.length()>0)){
			return false;
		} else {
			return true;
		}
	}

}
