package com.addcel.prosa.model.user;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.addcelexception.Error;
import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.components.toObject.Objectable;
import com.addcel.prosa.dto.ContestacionAlta;
import com.addcel.prosa.dto.Tienda;

public class OContestacion implements Objectable{

	public Object execute(String data) throws OwnException {
		
		
		try {
			
			JSONObject jsonObject = new JSONObject(data);

			ContestacionAlta contestacion = new ContestacionAlta();
			
			contestacion.setIdError(jsonObject.optInt("idError", 0)); // 2 cambiar contraseņa
			contestacion.setIdUser(jsonObject.optInt("idUser", 0));
			contestacion.setMensajeError(jsonObject.optString("mensajeError", null));
			
			return contestacion;
			
		} catch (JSONException e) {
			e.printStackTrace();
			throw new OwnException(Error.JSON_EXCEPTION);
		}
	}
}
