package com.addcel.prosa.model.userestatus;

import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.DataAccessible;
import com.addcel.api.dataaccess.components.connection.http.MethodPOST;
import com.addcel.api.ui.Viewable;
import com.addcel.prosa.model.URL;

public class DAUserEstatus extends DataAccessible implements Runnable{

	public DAUserEstatus(Viewable viewable, String data) {
		super(viewable, data);
	}

	public void run() {
		execute(viewable, data);
	}

	protected void execute(Viewable viewable, String json) {
		
		try {

			String encrypt = this.encrypt(CRYPTO_HARD, json);

			connectable = new MethodPOST(URL.URL_CAMBIAR_STATUS_USUARIOS);
			connectable.execute(encrypt);

			String httpAnswer = (String)connectable.getData();

			String decrypt = this.decrypt(CRYPTO_HARD, httpAnswer);
			
			OUserEstatus oUserEstatus = new OUserEstatus();
			Object object = oUserEstatus.execute(decrypt);

			viewable.setData(DataAccessible.DATA, object);

		} catch (OwnException e) {
			viewable.setData(DataAccessible.ERROR, e.toString());
		}
		
	}

}
