package com.addcel.api.dataaccess.components.toObject;

import com.addcel.api.addcelexception.OwnException;

public interface Objectable {

	public Object execute(String data) throws OwnException;
}
