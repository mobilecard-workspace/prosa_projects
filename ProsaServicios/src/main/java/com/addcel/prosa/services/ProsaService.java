package com.addcel.prosa.services;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.addcel.prosa.model.mapper.ProsaMapper;
import com.addcel.prosa.model.vo.AbstractResponse;
import com.addcel.prosa.model.vo.LoginVO;
import com.addcel.prosa.model.vo.TokenVO;
import com.addcel.prosa.model.vo.UserVO;
import com.addcel.prosa.model.vo.request.RequestBusquedaPagos;
import com.addcel.prosa.model.vo.request.RequestCatRoles;
import com.addcel.prosa.model.vo.request.RequestCatTienda;
import com.addcel.prosa.model.vo.request.RequestFindUser;
import com.addcel.prosa.model.vo.request.RequestVendedor;
import com.addcel.prosa.model.vo.response.LoginRespuesta;
import com.addcel.prosa.model.vo.response.TipoCambio;
import com.addcel.prosa.utils.AddCelGenericMail;
import com.addcel.prosa.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;

import crypto.Crypto;

@Service
public class ProsaService {
	private static final Logger logger = LoggerFactory
			.getLogger(ProsaService.class);
	private static final String URL_TIPO_CAMBIO="http://50.57.192.210:8080/Conversor/currency?fromCurrency=usd&toCurrency=MXN";
	private static final String URL_ENVIO_MAIL="http://localhost:8080/MailSenderAddcel/enviaCorreoAddcel";
	//private static final String URL_ENVIO_MAIL="http://50.57.192.210:8080/MailSenderAddcel/enviaCorreoAddcel";
	private static final String KEY_PASSWORD="1234567890ABCDEF0123456789ABCDEF";
	@Autowired
	private ProsaMapper mapper;
	@Autowired
	private UtilsService utilService;	
	
	public String addUser(String jsonEnc) {
		AbstractResponse res = new AbstractResponse();
		String jsonResEnc=null;
		res.setIdError(1);		
		try {
			UserVO usuario = (UserVO) utilService.jsonToObject(AddcelCrypto.decryptHard(jsonEnc), UserVO.class);
			usuario.setPassword(generaPassword());
			int id = mapper.addUser(usuario);			
				mapper.insertaModulosUsuario(usuario.getIdUser(), usuario.getIdRole(), usuario.getIdProveedor());
				
				AddCelGenericMail.sendMail(utilService.objectToJson(
						AddCelGenericMail.generatedMailPassword(usuario.getEmail(), usuario.getPassword(), usuario.getLogin())));
//				enviaPasswordMail(usuario.getPassword(),usuario.getEmail());
			res.setIdError(0);
			res.setMensajeError("Usuario guardado correctamente.");
			
			logger.info("Usuario guardado correctamente con rs ==> {} id ==> {} ", id,usuario.getIdUser());
		} catch (DuplicateKeyException pe) {
			res.setIdError(1);
			res.setMensajeError("El login de usuario ya existe");
			logger.error("Nombre de usuario duplicado: {}", pe.getMessage());
		}catch (Exception pe) {
			res.setIdError(2);
			res.setMensajeError("Error al registrar usuario");
			logger.error("Error al insertar usuario: {}", pe.getMessage());
		}finally{
			jsonResEnc = utilService.objectToJson(res);
			jsonResEnc = AddcelCrypto.encryptHard(jsonResEnc);
		}
		return jsonResEnc;
	}
	
	public String updateUser(String jsonEnc){				
		
		AbstractResponse res = new AbstractResponse();
		
		UserVO usuario=(UserVO) utilService.jsonToObject(AddcelCrypto.decryptHard(jsonEnc), UserVO.class);
		if(usuario.getIdUser()!=usuario.getIdSupervisor()){
			try{
				mapper.updateUser(usuario);		
				res.setIdError(0);
				res.setMensajeError("Actualización correcta");
				logger.info("Usuario actualizado: id ==> {} regAct==> {}",usuario.getIdUser());
			}catch(Exception e){
				res.setIdError(1);
				res.setMensajeError("Error al actualizar Usuario");
				logger.error("Error al actualizar usuario con id ==> {}. Error\n {}",usuario.getIdUser(),e);
			}						
		}else{
			res.setIdError(2);
			res.setMensajeError("Operación no permitida");
		}
		return AddcelCrypto.encryptHard(utilService.objectToJson(res));
	}

	public String login(String jsonEnc) {
		LoginRespuesta lr = new LoginRespuesta();
		String jsonResEnc=null;
		String json = AddcelCrypto.decryptHard(jsonEnc);
		
		LoginVO login = (LoginVO) utilService.jsonToObject(json, LoginVO.class);				
		login.setPassword(encriptaPassword(login.getPassword()));
		UserVO user = mapper.userIntentosLogin(login);		
		
		if(user != null){			
			if(user.getIdStatus() == 1){
					if(user.getPassword().equals(login.getPassword())){
						lr = mapper.loginUsuario(user);
						mapper.updateIntentosStatus(user.getIdUser(),user.getIdProveedor(), 0, "contadorLogin");//Reset loginCount=0
						
						//Revisar bien en que condiciones se manda el tipo de cambio
						String tcs=utilService.peticionUrlPostParams(URL_TIPO_CAMBIO, "");
						logger.info("====> {}",tcs);
						if(tcs != null){
							TipoCambio tc=(TipoCambio) utilService.jsonToObject(tcs, TipoCambio.class );
							lr.setTipoCambio(tc.getConversionRate());
						}
						
						lr.setIdError(0);
						lr.setMensajeError("Usuario válido");
						logger.info("Login del usuario con id ==> {}",lr.getIdUser());
					}else{												
						int contador = user.getLoginCount()+1;						
						if(user.getIdTienda() <= contador){//idTienda trae el numero maximo de intentos por rol
							logger.debug("intentos ==> {} = max_login ==> {}",contador,user.getIdTienda());
							//bloquear usuario
							mapper.updateIntentosStatus(user.getIdUser(),user.getIdProveedor(), 3, "status");//Status 3=bloqueado
							mapper.updateIntentosStatus(user.getIdUser(),user.getIdProveedor(), 0, "contador");//Reset loginCount=0							    
							lr.setIdError(4);
							lr.setMensajeError("El usuario ha sido bloqueado");
							logger.info("++++++++++++++++ Bloqueo de usuario PROSA +++++++++++++++++++");
							logger.info("# de intentos superado. El usuario con id ==> {} ha sido bloqueado ",user.getIdUser());
						}else{							
							mapper.updateIntentosStatus(user.getIdUser(),user.getIdProveedor(), contador, "contador");//incrementa el contador							
							lr.setIdError(5);
							lr.setMensajeError("Contraseña incorrecta");							
						}
					}									
			}else if(user.getIdStatus() == 2){
				lr.setIdError(2);
				lr.setIdUser(user.getIdUser());
				lr.setMensajeError("Se requiere cambio de contraseña");
				logger.info("Se requiere cambio de contraseña del usuario con id ==> {}",user.getIdUser());
			}else if(user.getIdStatus() == 3){
				lr.setIdError(3);
				lr.setMensajeError("Usuario bloqueado");
				logger.info("Usuario bloqueado id ==> {}",user.getIdUser());
			}else if(user.getIdStatus() == 4){
				lr.setIdError(4);
				lr.setMensajeError("Usuario dado de baja, favor de contactar con su Supervisor.");
				logger.info("Usuario dado de baja id ==> {}",user.getIdUser());
			}else{
				lr.setIdError(4);
				lr.setMensajeError("Status no valido");
				logger.info("Status no valido id ==> {}",user.getIdUser());
			}
		} else{
			lr.setIdError(1);
			lr.setMensajeError("El usuario no existe á é í ó ú ñ Ñ Á É Í Ö Ü");
			logger.error("Error al realizar login-usuario: {} ==> {}",
					login.getLogin(), login.getPassword());
		}	
		try{
			jsonResEnc=utilService.objectToJson(lr);
			jsonResEnc=AddcelCrypto.encryptHard(jsonResEnc);
		}catch(Exception e){
			logger.error("Error al realizar json-encoding:");
		}
			
		
		return jsonResEnc;		
	}
	
	public String listaTiendas(String data) {
		String dataBack = "";
		String json = AddcelCrypto.decryptHard(data);
		RequestCatTienda rct = (RequestCatTienda) utilService.jsonToObject(json, RequestCatTienda.class);
		dataBack = utilService.objectToJson(mapper.listaTiendas(rct.getProveedor()));
		dataBack = AddcelCrypto.encryptHard(dataBack);		
		return dataBack;
	}
	
	public String listaRoles(String data) {
		String dataBack = "";
		String json = AddcelCrypto.decryptHard(data);
		RequestCatRoles rcr = (RequestCatRoles)utilService.jsonToObject(json, RequestCatRoles.class);
		dataBack = utilService.objectToJson(mapper.getCatRoles(rcr.getId_aplicacion(), rcr.getId_proveedor()));
		dataBack = AddcelCrypto.encryptHard(dataBack);		
		return dataBack;
	}
	
	public String findUser(String data) {
		String dataBack = "";
		String json = AddcelCrypto.decryptHard(data);
		RequestFindUser request = (RequestFindUser) utilService.jsonToObject(json, RequestFindUser.class);
		request.setLogin(request.getLogin() != null && !"".equals( request.getLogin() )? request.getLogin() + "%": request.getLogin());
		request.setNombres(request.getNombres() != null && !"".equals( request.getNombres() )? request.getNombres() + "%": request.getNombres());
		request.setPaterno(request.getPaterno() != null && !"".equals( request.getPaterno() )? request.getPaterno() + "%": request.getPaterno());
		request.setMaterno(request.getMaterno() != null && !"".equals( request.getMaterno() )? request.getMaterno() + "%": request.getMaterno());
		dataBack = utilService.objectToJson(mapper.findUser(request.getNombres(), request.getPaterno(), request.getMaterno(), request.getLogin(), null, request.getIdProveedor(),null));
		dataBack = AddcelCrypto.encryptHard(dataBack);
		return dataBack;
	}
	
	public String getVendor(String data) {
		String dataBack = "";
		String json = AddcelCrypto.decryptHard(data);
		RequestVendedor request = (RequestVendedor) utilService.jsonToObject(json, RequestVendedor.class);
		dataBack = utilService.objectToJson(mapper.findUser(null,null,null,null, request.getIdSupervisor(), request.getIdProveedor(), request.getIdTienda()));
		return AddcelCrypto.encryptHard(dataBack);
	}
	
	public String generaToken(String json){				
		TokenVO tokenVO = null;
		try{
			tokenVO = (TokenVO) utilService.jsonToObject(AddcelCrypto.decryptHard(json), TokenVO.class);
			if(!"userPrueba".equals(tokenVO.getUsuario()) || !"passwordPrueba".equals(tokenVO.getPassword())){
				json="{\"idError\":2,\"mensajeError\":\"No tiene permisos para consumir este webservice.\"}";
			}else{
				json="{\"token\":\""+AddcelCrypto.encryptHard(mapper.getFechaActual())+"\",\"idError\":0,\"mensajeError\":\"\"}";
			}
		}catch(Exception e){
			json="{\"idError\":1,\"mensajeError\":\"Ocurrio un error al generar el Token.\"}";
		}
		json = AddcelCrypto.encryptHard(json);
				
		return json;
	}

	public String deleteUser(String jsonEnc){
		AbstractResponse res = new AbstractResponse(1, "Ocurrio un error al borrar el usuario");
		
		UserVO user = (UserVO) utilService.jsonToObject(AddcelCrypto.decryptHard(jsonEnc), UserVO.class);
		if(user.getIdUser()==user.getIdSupervisor()){
			res.setIdError(1);
			res.setMensajeError("Acción no permitida");
			logger.info("Se trato de borrar el mismo usuario: {}",user.getIdUser(), user.getIdSupervisor());
		}else{			
			mapper.updateIntentosStatus(user.getIdUser(),user.getIdProveedor(), 4, "status");
			res.setIdError(0);
			res.setMensajeError("Usuario borrado correctamente");
			logger.info("Se borro el usuario con id ==> {}, editado por usuario con id ==> {}",user.getIdUser(), user.getIdSupervisor());
		}	
		String jsonResEnc=utilService.objectToJson(res);
		jsonResEnc=AddcelCrypto.encryptHard(jsonResEnc);
		return jsonResEnc;
	}
	
	public String actualizaPassword(String jsonEnc,int status){
		/*String actPass="{\"idUser\":14,\"password\":\"cadena password\",\"idSupervisor\":14}";
		String deleteUser="{\"idUser\":14,\"idSupervisor\":1}";
		AddcelCrypto.encryptHard(actPass);
		deleteUser=AddcelCrypto.encryptHard(deleteUser);
		AddcelCrypto.decryptHard(deleteUser);*/
		AbstractResponse ar=new AbstractResponse();
		String json=AddcelCrypto.decryptHard(jsonEnc);
		String jsonResEnc = null;
//		UserVO user = (UserVO)utilService.jsonToObject(json, UserVO.class);
		LoginVO login = (LoginVO) utilService.jsonToObject(json, LoginVO.class);
		UserVO user = mapper.userIntentosLogin(login);		
		
		if(user == null){
					ar.setIdError(1);
					ar.setMensajeError("El usuario no existe");
					logger.error("Error al realizar actualizacion password-usuario: {} ==> {}", login.getLogin(), login.getPassword());
		}else{
			if(status==1){
				user.setIdStatus(1);//Cambio de cntraseña de usuario nuevo
			}
			if(user.getIdUser()==user.getIdSupervisor()){
				user.setPassword(encriptaPassword(user.getPassword()));
				mapper.updateUser(user);
				ar.setIdError(0);
				ar.setMensajeError("Actualización correcta");
			}else{
				ar.setIdError(1);
				ar.setMensajeError("Operación no permitida");
			}
			jsonResEnc=utilService.objectToJson(ar);
			jsonResEnc=AddcelCrypto.encryptHard(jsonResEnc);	
		}
		return jsonResEnc;
	}

	public String generaPassword() {
		String sPwd = (random(5869452) + random(5869452) + random(5869452)+ random(5869452)).substring(0, 10);
		logger.info("Password: {}",sPwd);
		sPwd = Crypto.aesEncrypt(KEY_PASSWORD, sPwd);				
		logger.info("Password enc: {}",sPwd);		
		return sPwd;
	}
	
	public String encriptaPassword(String password){
		logger.debug("password: {}",password);
		password = Crypto.aesEncrypt(KEY_PASSWORD, password);		
		logger.info("Password enc: {}",password);
		return password;
	}

	public String random(int n) {
		Random rand = new Random();
		return "" + rand.nextInt(n);
	}		

//	private void enviaPasswordMail(String psw,String mailTo){
//		CorreoVO correo = new CorreoVO();		
//		correo.setBody("La Contraseña para la aplicacion PROSA Movil es:: " + Crypto.aesDecrypt(KEY_PASSWORD,psw));
//		correo.setTo(new String []{mailTo});
//		correo.setSubject("Contraseña PROSA Movil");
//		String json = utilService.objectToJson(correo);
//		logger.debug("Json correo: {}",json);		
//		try {
//			URL url = new URL(URL_ENVIO_MAIL);			
//			HttpURLConnection con = (HttpURLConnection) url.openConnection();	
//			// Se indica que se escribira en la conexion
//			con.setDoOutput(true);
//			con.setRequestProperty("Content-Type", "application/json");
//			con.setRequestProperty("Accept", "application/json");
//			con.setRequestMethod("POST");			
//			
//			// Se escribe los parametros enviados a la url
//			OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());						
//			wr.write(json);
//			wr.close();					
//			logger.info("Codigo de respuesta MailSenderAddcel: {}",con.getResponseCode());				
//			con.disconnect();			
//		} catch (MalformedURLException ex) {
//			logger.error("Error MalformedURLException:"+ex);
//		} catch (IOException ex) {
//			logger.error("Error IOException:"+ex);
//		}
//	}
//	
	public String busquedaPagos(String data) {
		RequestBusquedaPagos request = 
				(RequestBusquedaPagos) utilService.jsonToObject(AddcelCrypto.decryptHard(data), RequestBusquedaPagos.class);
		return AddcelCrypto.encryptHard(utilService.objectToJson(
				mapper.getDetalle(null, request.getIdUser(), request.getIdProveedor(), null, request.getFi(), request.getFf(), null, null)));
	}
	
}
