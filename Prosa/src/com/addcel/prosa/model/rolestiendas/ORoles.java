package com.addcel.prosa.model.rolestiendas;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.addcelexception.Error;
import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.components.toObject.Objectable;
import com.addcel.prosa.dto.Rol;

public class ORoles implements Objectable{

	private Vector vector = null;
	
	public Object execute(String data) throws OwnException {
		
		try {
			
			JSONArray jsonArray = new JSONArray(data);

			int length = jsonArray.length();
			
			vector = new Vector();
			
			for(int index = 0; index < length; index++){
				
				JSONObject jsonObject = (JSONObject)jsonArray.get(index);
				
				System.out.println(jsonObject.toString());
				
				Rol rol = new Rol();
				
				rol.setIdRol(jsonObject.optInt("idRol", 0));
				rol.setMaxLogin(jsonObject.optInt("maxLogin", 0));
				rol.setIdAplicacion(jsonObject.optInt("idAplicacion", 0));
				rol.setDescripcion(jsonObject.optString("descripcion", null));
				
				vector.addElement(rol);
			}
			
			return vector;
			
		} catch (JSONException e) {
			e.printStackTrace();
			throw new OwnException(Error.JSON_EXCEPTION);
		}	
	}
}
