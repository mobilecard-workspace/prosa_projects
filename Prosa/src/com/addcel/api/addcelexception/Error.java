package com.addcel.api.addcelexception;

public class Error {

	public static String HTTP_SEND_DATA = "No hay conexión con el servicio.";
	public static String HTTP_RECEIVE_DATA = "No se recibio información del servicio.";
	public static String JSON_EXCEPTION = "No se pudo interpretar la contestación del servidor.";

	public static String NULLPOINTER_EXCEPTION = "No se obtuvo la información.";

	public static String EDITFIELDS_NO_COMPLETADOS = "Verificar los campos.";
	
	public static String PASSWORDS_IGUALES = "Las cadenas de caracteres deben de ser iguales.";
	//public static String LOGIN_EXCEPTION = "No se obtuvo la información.";
}
