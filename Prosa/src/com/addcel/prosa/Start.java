package com.addcel.prosa;

import net.rim.device.api.ui.UiApplication;

import com.addcel.api.ui.uicomponents.splash.Admin;
import com.addcel.api.util.UtilBB;
import com.addcel.prosa.dto.RespuestaLogin;

public class Start extends UiApplication
{

	static public String IDEAL_CONNECTION = null;
	static public RespuestaLogin respuestaLogin = null;
	
    public static void main(String[] args)
    {
    	Start theApp = new Start();       
        theApp.enterEventDispatcher();
    }
    

    public Start()
    {        
		IDEAL_CONNECTION = UtilBB.checkConnectionType();
        pushScreen(new Admin());
    }    
}
