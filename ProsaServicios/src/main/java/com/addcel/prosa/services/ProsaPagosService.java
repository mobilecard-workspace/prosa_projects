package com.addcel.prosa.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.prosa.model.mapper.BitacorasMapper;
import com.addcel.prosa.model.mapper.ProsaMapper;
import com.addcel.prosa.model.vo.ComisionVO;
import com.addcel.prosa.model.vo.ProsaDetalleVO;
import com.addcel.prosa.model.vo.ProsaPagoVO;
import com.addcel.prosa.model.vo.pagos.TBitacoraVO;
import com.addcel.prosa.model.vo.request.DatosPago;
import com.addcel.prosa.utils.AddCelGenericMail;
import com.addcel.prosa.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;

@Service
public class ProsaPagosService {
	private static final Logger logger = LoggerFactory.getLogger(ProsaPagosService.class);
	private static final String URL_AUT_PROSA = "http://localhost:8080/ProsaWeb/ProsaAuth?";
	private static final String URL_TIPO_CAMBIO="http://localhost:8080/Conversor/currency?fromCurrency=usd&toCurrency=MXN";
	
//	private static final String URL_AUT_PROSA = "http://50.57.192.213:8080/ProsaWeb/ProsaAuth";
//	private static final String URL_REV_PROSA = "http://50.57.192.213:8080/ProsaWeb/ProsaRev";
	@Autowired 
	private ProsaMapper mapper;
	@Autowired
	private UtilsService utilService;	
	@Autowired
	private BitacorasMapper bm;
	@Autowired
	private ProsaMapper mapperDF;

	
	private static final String patron = "ddhhmmss";
	private static final String patronCom = "yyyy-MM-dd hh:mm:ss";
	private static final SimpleDateFormat formatoCom = new SimpleDateFormat(patronCom);
	private static final SimpleDateFormat formato = new SimpleDateFormat(patron);
	
	public String procesaPago(String jsonEnc){		
		DatosPago datosPago = null;
		String json = null;
		ProsaPagoVO prosaPagoVO = null;
//		long idBitacora = 0;
//		String token = null;
		
		try{
//			token=mapper.getFechaActual();
			json=AddcelCrypto.decryptSensitive(jsonEnc);		
			logger.debug("jsondatospago: {}",json);
			datosPago = (DatosPago) utilService.jsonToObject(json, DatosPago.class);
			datosPago.setTipo("VISA");
			
			if(datosPago.getToken() == null){
				json = "{\"idError\":1,\"mensajeError\":\"El parametro TOKEN no puede ser NULL\"}";
				logger.error("El parametro TOKEN no puede ser NULL");
			}else{
				datosPago.setToken(AddcelCrypto.decryptHard(datosPago.getToken()));
				logger.info("token ==> {}",datosPago.getToken());
				
				if((mapperDF.difFechaMin(datosPago.getToken())) > 30){
//				if(false){
					json = "{\"idError\":2,\"mensajeError\":\"La Transacción ya no es válida\"}";
					logger.info("La Transacción ya no es válida");
				}else{
//				if(true){
					//Insetar Bitacoras Prosa y detalle						
					logger.info("Inserta Bitacoras VISA.");
					insertaBitacoras(datosPago, datosPago.getTotal(), 12.0);
					
					try{
						StringBuffer data = new StringBuffer() 
								.append( "card="      ).append( URLEncoder.encode(datosPago.getTarjeta(), "UTF-8") )
								.append( "&vigencia=" ).append( URLEncoder.encode(datosPago.getVigencia(), "UTF-8") )
								.append( "&nombre="   ).append( URLEncoder.encode(datosPago.getNombres(), "UTF-8") )
								.append( "&cvv2="     ).append( URLEncoder.encode(datosPago.getCvv2(), "UTF-8") )
								.append( "&monto="    ).append( URLEncoder.encode(datosPago.getTotal() + "", "UTF-8") )
								.append( "&afiliacion=" ).append( URLEncoder.encode("000000", "UTF-8") )
								.append( "&moneda="   ).append( URLEncoder.encode("MXN", "UTF-8")) ;
								
						logger.info("Envío de datos VISA: {}", data);
						
						URL url = new URL(URL_AUT_PROSA);
						URLConnection urlConnection = url.openConnection();
						
						urlConnection.setDoOutput(true);
						
						OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
						wr.write(data.toString());
						wr.flush();
						logger.info("Datos enviados, esperando respuesta de PROSA");
						
						BufferedReader rd = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
						String line = null;
						StringBuilder sb = new StringBuilder();
						
						while ((line = rd.readLine()) != null) {
							sb.append(line);
						}
						
						wr.close();
						rd.close();
						
						prosaPagoVO = (ProsaPagoVO) utilService.jsonToObject(sb.toString(), ProsaPagoVO.class);
						
						if(prosaPagoVO != null){
							datosPago.setNoAutorizacion(prosaPagoVO.getAutorizacion());
							datosPago.setReferencia(prosaPagoVO.getTransactionId());
							datosPago.setFecha(formatoCom.format(new Date()));
							datosPago.setTransaccion(String.valueOf(datosPago.getIdBitacora()));
//							datosPago.setMsg(prosaPagoVO.getMsg());
							
							if(prosaPagoVO.isAuthorized()){
								datosPago.setStatus(1);
								datosPago.setMsg("EXITO PAGO PROSA VISA AUTORIZADA");
								json = "{\"idError\":0,\"mensajeError\":\"El pago fue exitoso.\",\"referencia\":" + prosaPagoVO.getTransactionId() + 
										",\"autorizacion\":" + prosaPagoVO.getAutorizacion() +"}";
								
								AddCelGenericMail.sendMail(utilService.objectToJson(AddCelGenericMail.generatedMail(datosPago)));
								
							}else if(prosaPagoVO.isRejected()){
								datosPago.setStatus(3);
								datosPago.setMsg("PAGO PROSA VISA RECHAZADA");
								json = "{\"idError\":4,\"mensajeError\":\"El pago fue rechazado.\",\"referencia\":" + prosaPagoVO.getTransactionId() + "}";
								
							}else if(prosaPagoVO.isProsaError()){
								datosPago.setStatus(2);
								datosPago.setMsg("PAGO PROSA VISA ERROR");
								json = "{\"idError\":5,\"mensajeError\":\"Ocurrio un error durante el pago Banco.\",\"referencia\":" + prosaPagoVO.getTransactionId() + "}";
							}
							
							updateBitacoras(datosPago);
						}
						
					}catch(Exception e){
						logger.error("Ocurrio un error durante el pago al banco:", e);
						datosPago.setStatus(2);
						datosPago.setMsg("Ocurrio un error durante el pago al banco");
						json = "{\"idError\":6,\"mensajeError\":\"Ocurrio un error durante el pago.\"}";
						updateBitacoras(datosPago);
					}
				}
				
			}
		}catch(Exception e){
			logger.error("Ocurrio un error:", e);
			datosPago.setMensajeError("Ocurrio un error: " + e.getMessage());
			datosPago.setStatus(2);
			datosPago.setMsg("Ocurrio un error");
			updateBitacoras(datosPago);
			json = "{\"idError\":3,\"mensajeError\":\"Ocurrio un error: " + e.getMessage() + "\"}";
		}
		json = AddcelCrypto.encryptSensitive(formato.format(new Date()),json);
		return json;
	}	
	
	private long insertaBitacoras(DatosPago dp, Double total, Double tipoCambio ){		
		TBitacoraVO tb = new TBitacoraVO(
				dp.getIdUser(),dp.getIdProveedor(), "PAGO PROSA VISA", dp.getImei(), "PAGO PROSA VISA",
				null,dp.getTipo(), dp.getSoftware(), dp.getModelo(),dp.getWkey());
		bm.addBitacora(tb);
		ProsaDetalleVO dfd = new ProsaDetalleVO(
				tb.getIdBitacora(), dp.getIdUser(),dp.getIdProveedor() ,dp.getDescProducto(),  
				total, dp.getMoneda(), tipoCambio,"VISA", dp.getNombres(), dp.getEmail());
		bm.addProsaDetalle(dfd);
		dp.setIdBitacora(tb.getIdBitacora());
		return tb.getIdBitacora();
	}
	
	private void updateBitacoras(DatosPago dp){
		TBitacoraVO tb = new TBitacoraVO();
		
		tb.setIdBitacora(dp.getIdBitacora());
		tb.setBitStatus(dp.getStatus());
		tb.setBitConcepto(dp.getMsg());
		tb.setBitNoAutorizacion(dp.getNoAutorizacion());
		bm.updateBitacora(tb);
		// idStatus
		bm.updateProsaDetalle(dp.getIdBitacora(), dp.getStatus());
	}
	
	private Double calculaTotal(ComisionVO comisiones,Double subTotal){
		Double comision,total;		
		if(subTotal > comisiones.getMinComPorcentaje() && subTotal< comisiones.getMinCom() ){
			comision=subTotal*comisiones.getComisionPorcentaje();
			total=subTotal+comision;
			logger.info("******* Calculo de pago DuttyFree *******");
			logger.debug("Pago por comision %");
			logger.info("Subtotal: {}",subTotal);
			logger.info("% Comision: {}",comisiones.getComisionPorcentaje());
			logger.info("Comision : {}",comision);
			logger.info("Total: {}",total);
			logger.info("+++++++++++++++++++++++++++++++++++++++++");						
		}else{//Se aplica comision fija										
			total=subTotal+comisiones.getComision();
			logger.info("******* Calculo de pago DuttyFree *******");
			logger.debug("Pago por comision fija");
			logger.info("Subtotal: {}",subTotal);
			logger.info("Comision Fija: {}",comisiones.getComision());					
			logger.info("Total: {}",total);
			logger.info("+++++++++++++++++++++++++++++++++++++++++");			
		}
		return total;
	}
}
