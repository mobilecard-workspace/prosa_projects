package com.addcel.prosa.view;

import java.util.Vector;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;

import com.addcel.api.ui.Viewable;
import com.addcel.prosa.Start;
import com.addcel.prosa.dto.Modulo;
import com.addcel.prosa.view.base.uicomponents.menu.CustomGridFieldMenu;

public class WMenuVendedor extends Viewable implements FieldChangeListener{

	public WMenuVendedor(boolean isSetTitle, String title) {
		
		super(isSetTitle, title);
		
		Vector modulos = Start.respuestaLogin.getModulos();
		
		if (modulos != null){
			
			int size = modulos.size();
			
			for(int index = 0; index < size; index++){
				
				Modulo modulo = (Modulo)modulos.elementAt(index);
				CustomGridFieldMenu option01 = new CustomGridFieldMenu(modulo.getDescripcion(), modulo.getIdModule());
				add(option01);
			}
		}
	}

	public void fieldChanged(Field field, int context) {
	}

	protected void analyzeData(int request, Object object) {
	}

}
