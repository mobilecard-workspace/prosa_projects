package com.addcel.prosa.model.login;

import com.addcel.api.dataaccess.components.descryption.Descryptionable;
import com.addcel.api.util.security.AddcelCrypto;


public class DescryptLogin implements Descryptionable{

	private String data;
	
	public void execute(String data) {

		this.data = AddcelCrypto.decryptHard(data);
	}

	public Object getData() {

		return data;
	}



}
