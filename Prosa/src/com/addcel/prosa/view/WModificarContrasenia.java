package com.addcel.prosa.view;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BasicEditField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

import com.addcel.api.addcelexception.Error;
import com.addcel.api.dataaccess.DataAccessible;
import com.addcel.api.ui.Viewable;
import com.addcel.api.ui.uicomponents.CustomEditField;
import com.addcel.api.ui.uicomponents.CustomPasswordEditField;
import com.addcel.api.ui.uicomponents.ElementLabelField;
import com.addcel.api.ui.uicomponents.buttons.CustomSelectedButtonField;
import com.addcel.prosa.Start;
import com.addcel.prosa.dto.ContestacionAlta;
import com.addcel.prosa.model.modificarcontrasenia.DAModificarContrasenia;

public class WModificarContrasenia extends Viewable implements FieldChangeListener{

	private CustomEditField usuarioEdit;
	
	private CustomPasswordEditField passEdit01;
	private CustomPasswordEditField passEdit02;
	
	private ElementLabelField usuarioTxt;
	private ElementLabelField passTxt01;
	private ElementLabelField passTxt02;
	
	private CustomSelectedButtonField aceptar;
	private CustomSelectedButtonField cancelar;
	
	public WModificarContrasenia(boolean isSetTitle, String title) {
		super(isSetTitle, title);

		usuarioEdit = new CustomEditField();
		
		passEdit01 = new CustomPasswordEditField();
		passEdit02 = new CustomPasswordEditField();

		usuarioTxt = new ElementLabelField("Nombre de usuario");
		passTxt01 = new ElementLabelField("Nueva contraseņa");
		passTxt02 = new ElementLabelField("Confirmar nueva contraseņa");
		
		aceptar = new CustomSelectedButtonField("Aceptar", ButtonField.CONSUME_CLICK);
		cancelar = new CustomSelectedButtonField("Cancelar", ButtonField.CONSUME_CLICK);

		aceptar.setChangeListener(this);
		cancelar.setChangeListener(this);
		
		HorizontalFieldManager fieldManager = new HorizontalFieldManager();
		
		add(usuarioTxt);
		add(usuarioEdit);
		
		add(passTxt01);
		add(passEdit01);
		add(passTxt02);
		add(passEdit02);
		add(new LabelField(""));
		fieldManager.add(cancelar);
		fieldManager.add(aceptar);
		add(fieldManager);
	}

	protected void analyzeData(int request, Object object) {
		
		if (request == DataAccessible.ERROR){
			
			String message = cleanMessageError(object);
			Dialog.alert(message);
		} else {
			
			ContestacionAlta contestacionAlta = (ContestacionAlta)object;
			String message = contestacionAlta.getMensajeError();
			Start.respuestaLogin.setPassword(passEdit01.getText());
			Dialog.alert(message);
		}
	}

	public void fieldChanged(Field field, int context) {

		if (field == cancelar){
			
			UiApplication.getUiApplication().popScreen(this);
		} else if (field == aceptar){
			
			if (checarCampos()){
				
				String pass01 = passEdit01.getText();
				String pass02 = passEdit02.getText();
				
				if (pass01.equals(pass02)){
					
					int idUser = Start.respuestaLogin.getIdUser();
					
					JSONObject jsonObject = new JSONObject();
					
					try {
						
						jsonObject.put("idUser", idUser);
						jsonObject.put("idProveedor", 20);
						jsonObject.put("idSupervisor", idUser);
						jsonObject.put("login", usuarioEdit.getText());
						jsonObject.put("password", pass01);
						
						Thread thread = new Thread(new DAModificarContrasenia(this, jsonObject.toString()));
						thread.start();
						
					} catch (JSONException e) {
						e.printStackTrace();
						Dialog.alert(Error.JSON_EXCEPTION);
					}
					
				} else {
					Dialog.alert(Error.PASSWORDS_IGUALES);
				}
			} else {

				Dialog.alert(Error.EDITFIELDS_NO_COMPLETADOS);
			}
		}
		

	}
	
	
	private boolean checarCampos(){
		
		boolean check = true;
		
		if (invalidTextUser(usuarioEdit)){
			check = false;
		} else if (invalidText(passEdit01)){
			check = false;
		} else if (invalidText(passEdit02)){
			check = false;
		}
		
		return check;
	}
	

	private boolean invalidText(BasicEditField editField){
		
		String temp = editField.getText();
		
		if ((temp != null)&&(temp.length()>=8)&&(temp.length()<=12)){
			return false;
		} else {
			return true;
		}
	}

	private boolean invalidTextUser(BasicEditField editField){
		
		String temp = editField.getText();
		
		if ((temp != null)&&(temp.length()>0)){
			return false;
		} else {
			return true;
		}
	}
}