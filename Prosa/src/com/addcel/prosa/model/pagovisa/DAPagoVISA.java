package com.addcel.prosa.model.pagovisa;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.addcelexception.Error;
import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.DataAccessible;
import com.addcel.api.dataaccess.components.connection.http.MethodPOST;
import com.addcel.api.ui.Viewable;
import com.addcel.api.util.UtilBB;
import com.addcel.prosa.Start;
import com.addcel.prosa.dto.Token;
import com.addcel.prosa.model.URL;

public class DAPagoVISA extends DataAccessible implements Runnable{

	public DAPagoVISA(Viewable viewable, String data) {
		super(viewable, data);
	}

	
	public DAPagoVISA(Viewable viewable, JSONObject jsonObject) {
		super(viewable, jsonObject);
	}
	
	public void run() {
		
		execute(viewable, data);
	}

	protected void execute(Viewable viewable, String json) {

		try {
			
			try {
				
				JSONObject jsonToken = new JSONObject();
				
				jsonToken.put("idProveedor", 20);
				jsonToken.put("usuario", Start.respuestaLogin.getUser());
				jsonToken.put("password", Start.respuestaLogin.getPassword());

				
				String TEMP = "{\"idProveedor\":1,\"usuario\":\"userPrueba\",\"password\":\"passwordPrueba\" }";
				
				
				String encrypt = this.encrypt(CRYPTO_HARD, TEMP);
				
				connectable = new MethodPOST(URL.URL_GET_TOKEN);
				connectable.execute(encrypt);
				
				String httpAnswer = (String)connectable.getData();
				
				String decrypt = this.decrypt(CRYPTO_HARD, httpAnswer);
				
				OToken oToken = new OToken();
				Token token = (Token)oToken.execute(decrypt);
				
				
				jsonObject.put("token", token.getToken());
				jsonObject.put("imei", UtilBB.getImei());
				jsonObject.put("modelo", UtilBB.getDeviceINFO(UtilBB.DEVICE_NAME));
				jsonObject.put("software", UtilBB.getDeviceINFO(UtilBB.SOFTWARE_VERSION));
				
				encrypt = this.encrypt(CRYPTO_SENS, jsonObject.toString());
				
				connectable = new MethodPOST(URL.URL_PAGO_VISA);
				connectable.execute(encrypt);
				
				httpAnswer = (String)connectable.getData();
				
				decrypt = this.decrypt(CRYPTO_SENS, httpAnswer);
				
				OPagoVisa pagoVisa = new OPagoVisa();
				Object object = pagoVisa.execute(decrypt);
				viewable.setData(DataAccessible.DATA, object);
				
			} catch (JSONException e) {
				e.printStackTrace();
				viewable.setData(DataAccessible.ERROR, Error.JSON_EXCEPTION);
			}
			
		} catch (OwnException e) {
			viewable.setData(DataAccessible.ERROR, e.toString());
		}
	}

}
